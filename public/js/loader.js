window.Loader = {
    loadImages: function() {
        var lazyLoadImages = window.document.getElementsByClassName('lazy-load');

        for (var i = 0; i < lazyLoadImages.length; i++) {
            var imageElement = lazyLoadImages[i];
            var originalSource = imageElement.getAttribute('data-lazy');

            var imagePreLoader = new Image();

            imagePreLoader.src = originalSource;
            imagePreLoader.setAttribute('data-index', i);

            imagePreLoader.onload = function (event) {
                if (event.path[0]) {
                    var loadedImage = event.path[0];
                    var loadedIndex = loadedImage.getAttribute('data-index');
                    lazyLoadImages[loadedIndex].src = loadedImage.src;
                }
            };
        }
    },

    loadApp() {
        var preLoader = document.getElementById('pre-loader');

        if (preLoader) {
            preLoader.parentNode.removeChild(preLoader);
            document.body.classList.remove('no-scroll');
        }
    }
};

window.onload = function () {
    window.Loader.loadApp();

    setTimeout(function () {
        window.Loader.loadImages();
    }, 500);
};

