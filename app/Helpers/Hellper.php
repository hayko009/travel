<?php

namespace App\Helpers;

use App\Models\Airline;
use App\Models\City;
use App\Models\Country;
use App\Models\Airport;
use App\Models\Flight;
use App\Models\Photo;
use App\Models\SiteSetting;
use App\Models\Video;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

/**
 * Class Helper
 * @package App\Hellpers
 */
class Helper
{
    /**
     * @var int
     */
    protected static $buildGenerationLimit = 6;

    /**
     * @var int
     */
    protected static $buildGenerationFlightsLimit = 9;

    /**
     * @var int
     */
    protected static $limit = 15;

    /**
     * @param bool $buildGeneration
     * @param null $countryCode
     * @param null $cityCode
     * @param null $airportCode
     * @param null $airlineCode
     * @param null $fromCode
     * @param null $toCode
     *
     * @return array
     */
    public static function generateRandomLinks(
        $buildGeneration = false,
        $countryCode = null,
        $cityCode = null,
        $airportCode = null,
        $airlineCode = null,
        $fromCode = null,
        $toCode = null
    ) {
        $randomLinks = [
            [
                'listName' => 'countries',
                'items'    => self::getRandomCountries($countryCode, $buildGeneration)
            ],
            [
                'listName' => 'cities',
                'items'    => self::getRandomCities($cityCode, $buildGeneration)
            ],
            [
                'listName' => 'airlines',
                'items'    => self::getRandomAirlines($airlineCode, $buildGeneration)
            ],
            [
                'listName' => 'airports',
                'items'    => self::getRandomAirports($airportCode, $buildGeneration)
            ],
        ];

        $randomLinks = [
            'randomLinks'   => $randomLinks,
            'randomFlights' => self::getRandomFlights($fromCode, $toCode, $buildGeneration)
        ];

        return $randomLinks;
    }

    /**
     * Get Random Airlines
     *
     * @param null    $code
     * @param boolean $buildGeneration
     *
     * @return array
     */
    public static function getRandomAirlines(
        $code = null,
        $buildGeneration = false
    ) {
        $randomAirlines = Airline::where('img', '!=', 'default.jpg')->inRandomOrder();

        if ($code) {
            $randomAirlines = $randomAirlines->where('code', '!=', $code);
        }

        return $randomAirlines->limit($buildGeneration ? self::$buildGenerationLimit : self::$limit)->get();
    }

    /**
     * Get Random airports
     *
     * @param null    $code
     * @param boolean $buildGeneration
     *
     * @return array
     */
    public static function getRandomAirports(
        $code = null,
        $buildGeneration = false
    ) {
        $randomAirports = Airport::inRandomOrder();
        if ($code) {
            $randomAirports = $randomAirports->where('code', '!=', $code);
        }

        return $randomAirports->limit($buildGeneration ? self::$buildGenerationLimit : self::$limit)->get();
    }

    /**
     * Get Random Cities
     *
     * @param null    $code
     * @param boolean $buildGeneration
     *
     * @return array
     */
    public static function getRandomCities(
        $code = null,
        $buildGeneration = false
    ) {
        $randomCities = City::whereNotNull('img')->inRandomOrder();
        if ($code) {
            $randomCities = $randomCities->where('code', '!=', $code);
        }

        return $randomCities->limit($buildGeneration ? self::$buildGenerationLimit : self::$limit)->get();
    }

    /**
     * Get Random Countries
     *
     * @param null    $code
     * @param boolean $buildGeneration
     *
     * @return array
     */
    public static function getRandomCountries(
        $code = null,
        $buildGeneration = false
    ) {
        $randomCountries = Country::inRandomOrder();
        if ($code) {
            $randomCountries = $randomCountries->where('code', '!=', $code);
        }

        return $randomCountries->limit($buildGeneration ? self::$buildGenerationLimit : self::$limit)->get();
    }

    /**
     * Get Random Flights
     *
     * @param City|null $from
     * @param City|null $to
     * @param bool      $buildGeneration
     *
     * @return array
     */
    public static function getRandomFlights(
        $from = null,
        $to = null,
        $buildGeneration = false
    ) {
        $flights = [];
        $cities = City::inRandomOrder()
            ->whereNotNull('img');
        if ($from && $to) {
            $cities = $cities->whereNotIn('code', [$from->code, $to->code]);
        }

        if ($buildGeneration) {
            $cities = $cities->limit(self::$buildGenerationFlightsLimit * 2)->get();

            foreach ($cities as $key => $city) {
                if ($key == self::$buildGenerationFlightsLimit) {
                    break;
                }
                $flights[] = [
                    'from' => $city,
                    'to'   => $cities[count($cities) - 1 - $key],
                ];

            }
        } else {
            $cities = $cities->limit(self::$limit * 2)->get();
            $flights['fromCombinations'] = [];
            $flights['toCombinations'] = [];

            // Generate Random flights
            foreach ($cities as $key => $city) {
                if ($key < self::$limit) {
                    $flights['fromCombinations'][] = [
                        'from' => $from,
                        'to'   => $city,
                    ];
                } else {
                    $flights['toCombinations'][] = [
                        'from' => $city,
                        'to'   => $to,
                    ];
                }
            }
        }
        return $flights;
    }

    /**
     * @param       $page
     * @param       $pagePrefix
     * @param       $count
     * @param       $limit
     * @param array $where
     *
     * @return array
     * @throws \Exception
     */
    public static function pagination($page, $pagePrefix, $count, $limit, $where = [])
    {
        $requestUrl = Request::segment(1);
        $pagination = [
            'page' => $page
        ];

        if (!is_string($pagePrefix)) {
            $model = $pagePrefix;
            $pagePrefix = $pagePrefix->code;
        }

        if ($page * $limit >= $count) {
            if (($page - 1) <= 0) {
                $pagination['prev']['url'] = url('/'.$requestUrl.'/'.$pagePrefix.'/1');
                $pagination['prev']['page'] = 1;
            } else {
                $pagination['prev']['url'] = url('/'.$requestUrl.'/'.$pagePrefix.'/'.($page - 1));
                $pagination['prev']['page'] = ($page - 1);
            }

            if (!empty($model)) {
                $modelName = class_basename($model);
                $pagePrefixNext = Factory::get("App\\Models\\".$modelName);
                foreach ($where as $key => $value) {
                    if ($value === 'notNull') {
                        $pagePrefixNext = $pagePrefixNext->whereNotNull($key);
                    } else {
                        $pagePrefixNext = $pagePrefixNext->where($key, $value);
                    }
                }

                if ($pagePrefixNext = $pagePrefixNext->where('id', '<', $model->id)->orderBy('id', 'desc')->first()) {
                    $pagePrefixNextData = $pagePrefixNext;
                    $pagePrefixNext = $pagePrefixNext->code;
                }
            }

            if (!empty($pagePrefixNext)) {
                $pagePrefix = $pagePrefixNext;
                $pagination['next']['data'] = $pagePrefixNextData;
                $pagination['next']['page'] = 1;
                $pagination['next']['url'] = url('/'.$requestUrl.'/'.$pagePrefix.'/1');
            } else {
                $pagination['next'] = false;
            }
        } elseif ($page <= 1) {
            $pagination['next']['url'] = url('/'.$requestUrl.'/'.$pagePrefix.'/'.($page + 1));
            $pagination['next']['page'] = ($page + 1);

            if (!empty($model)) {
                $modelName = class_basename($model);
                $pagePrefixPrev = Factory::get("App\\Models\\".$modelName);
                foreach ($where as $key => $value) {
                    if ($value === 'notNull') {
                        $pagePrefixPrev = $pagePrefixPrev->whereNotNull($key);
                    } else {
                        $pagePrefixPrev = $pagePrefixPrev->where($key, $value);
                    }
                }

                if ($pagePrefixPrev = $pagePrefixPrev->where('id', '>', $model->id)->orderBy('id', 'desc')->first()) {
                    $pagePrefixPrevData = $pagePrefixPrev;
                    $pagePrefixPrev = $pagePrefixPrev->code;
                }
            }

            if (!empty($pagePrefixPrev)) {
                $pagePrefix = $pagePrefixPrev;
                $pagination['prev']['data'] = $pagePrefixPrevData;
                $pagination['prev']['page'] = ceil($count / $limit);
                $pagination['prev']['url'] = url('/'.$requestUrl.'/'.$pagePrefix.'/'.ceil($count / $limit));
            } else {
                $pagination['prev'] = false;
            }
        } else {
            $pagination['prev']['url'] = url('/'.$requestUrl.'/'.$pagePrefix.'/'.($page - 1));
            $pagination['prev']['page'] = ($page - 1);
            $pagination['next']['url'] = url('/'.$requestUrl.'/'.$pagePrefix.'/'.($page + 1));
            $pagination['next']['page'] = ($page + 1);
        }

        return $pagination;
    }

    /**
     * Generate Video Path
     *
     * @param $video
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public static function generateVideoPath($video)
    {
        switch ($video->type) {
            case Video::$types['city']:
                return url('/videos/cities/'.$video->path);
                break;
            case Video::$types['country']:
                return url('/videos/countries/'.$video->path);
                break;
            case Video::$types['logo']:
                return url('/videos/logos/'.$video->path);
                break;
            case Video::$types['other']:
                return url('/videos/others/'.$video->path);
                break;
            default:
                return url('/videos/others/'.$video->path);
                break;
        }
    }

    /**
     * Generate Photo Path
     *
     * @param $photo
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public static function generatePhotoPath($photo)
    {
        switch ($photo->type) {
            case Photo::$types['city']:
                return url('/photos/cities/'.$photo->path);
                break;
            case Photo::$types['country']:
                return url('/photos/countries/'.$photo->path);
                break;
            case Photo::$types['logo']:
                return url('/photos/logos/'.$photo->path);
                break;
            case Photo::$types['other']:
                return url('/photos/others/'.$photo->path);
                break;
            default:
                return url('/photos/others/'.$photo->path);
                break;
        }
    }

    /**
     * Set locale and template
     * @param $domain
     */
    public static function setDomainConfings($domain)
    {
        switch ($domain) {
            case 'onetwoclick.esy.es':
                App::setLocale('ru');
                Config::set('template', 'templates.default');
                Config::set('currency', 'rub');
                break;
            case 'test.loc':
                App::setLocale('ru');
                Config::set('template', 'templates.default');
                Config::set('currency', 'rub');
                break;
            case 'www.travel.mk':
                App::setLocale('ru');
                Config::set('template', 'templates.tripxg');
                Config::set('currency', 'usd');
                break;
            case 'travel.hk':
                App::setLocale('ru');
                Config::set('template', 'templates.default');
                Config::set('currency', 'rub');
                break;
            default:
                App::setLocale('ru');
                Config::set('template', 'templates.tripxg');
                Config::set('currency', 'usd');
        }
    }
}
