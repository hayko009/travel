<?php

namespace App\Helpers;

use Exception;
use ReflectionClass;

/**
 * Factory provides the overridable instance or class name of entities.
 * Factory checks if requested entity exists in Addons first, then in App.
 * @package    ucraft
 * @subpackage services
 */
class Factory
{
    /**
     * Container for already created instances
     * @var array
     */
    protected static $instanceContainer = [];

    /**
     * Gets the instance of class.
     * It asks for the class from getClass function.
     *
     * @param string $className        Class Name for which object need to be created
     * @param bool   $forceNewInstance Force to return new instance of
     * @param mixed  $params           Parameters to be loaded in class object
     *
     * @return mixed
     * @throws Exception
     */
    public static function get(string $className, bool $forceNewInstance = false, ...$params)
    {
        // Get the class name
        $class = self::getClass($className);

        // Check if it must act as singleton or not and return new instance or already saved instance
        if ($forceNewInstance || !isset(self::$instanceContainer[$class])) {
            self::$instanceContainer[$class] = (new ReflectionClass($class))->newInstanceArgs($params);
        }

        return self::$instanceContainer[$class];
    }

    /**
     * Gets class name
     *
     * @param string $className
     *
     * @return string
     * @throws Exception
     */
    public static function getClass(string $className): string
    {

        // Check if a class exists return it, if not return native class name
        if (class_exists($className)) {
            return $className;
        } else {
            throw new Exception('Invalid Class Name: '.$className);
        }
    }
}