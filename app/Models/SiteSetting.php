<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteSetting
 * @package App\Models
 */
class SiteSetting extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'key',
        'value'
    ];

    /**
     * Cities Count
     * @return int
     */
    public static function citiesCount()
    {
        if (!empty($count = self::where('key', 'citiesCount')->first())) {
            return $count->value;
        }
        return 0;
    }

    /**
     * Full(img,exists_cases) cities Count
     * @return int
     */
    public static function fullCitiesCount()
    {
        if (!empty($count = self::where('key', 'fullCitiesCount')->first())) {
            return $count->value;
        }
        return 0;
    }

    /**
     * Countries Count
     * @return int
     */
    public static function countriesCount()
    {
        if (!empty($count = self::where('key', 'countriesCount')->first())) {
            return $count->value;
        }
        return 0;
    }

    /**
     * Airlines Count
     * @return int
     */
    public static function airlinesCount()
    {
        if (!empty($count = self::where('key', 'airlinesCount')->first())) {
            return $count->value;
        }
        return 0;
    }

    /**
     * Airports Count
     * @return int
     */
    public static function airportsCount()
    {
        if (!empty($count = self::where('key', 'airportsCount')->first())) {
            return $count->value;
        }
        return 0;
    }

    /**
     * Flights Count
     * @return int
     */
    public static function flightsCount()
    {
        if (!empty($count = self::where('key', 'flightsCount')->first())) {
            return $count->value;
        }
        return 0;
    }
}
