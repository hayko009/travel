<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * Class City
 * @package App\Models
 */
class City extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'params',
        'cases',
        'ordering',
        'img'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'params'  => 'array',
        'name'  => 'array',
        'cases' => 'array',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'name',
        'cases',
    ];

    /**
     * Get Attribute Name
     * @param $name
     * @return mixed
     */
    public function getNameAttribute($name)
    {
        $name = json_decode($name, true);

        if (!empty($name[App::getLocale()])) {
            return $name[App::getLocale()];
        }

        return $name[env('DEFAULT_LANGUAGE')];
    }

    /**
     * Get Attribute Cases
     * @param $cases
     * @return mixed
     */
    public function getCasesAttribute($cases)
    {
        $cases = json_decode($cases, true);

        if (!empty($cases[App::getLocale()])) {
            return $cases[App::getLocale()];
        }

        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function flights()
    {
        return $this->belongsToMany(City::class, 'flights', 'from_id','to_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function airports()
    {
        return $this->hasMany(Airport::class);
    }
}
