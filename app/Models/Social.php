<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $fillable = [
        'type',
        'type_id',
        'social',
    ];

    /**
     * Social data types
     * @var array
     */
    public static $types = [
        'video' => 1,
        'image' => 2,
    ];

    /**
     * Social
     * @var array
     */
    public static $socials = [
        'fb' => 1,
    ];
}
