<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * Class Airline
 * @package App\Models
 */
class Airline extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'img',
        'ordering'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'name'  => 'array',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'name'
    ];

    /**
     * Get Attribute Name
     * @param $name
     * @return mixed
     */
    public function getNameAttribute($name)
    {
        $name = json_decode($name, true);

        if (!empty($name[App::getLocale()])) {
            return $name[App::getLocale()];
        }

        return $name[env('DEFAULT_LANGUAGE')];
    }
}
