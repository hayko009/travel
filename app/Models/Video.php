<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'type',
        'city',
        'country',
        'locale',
        'path',
        'youtube',
        'vimeo',
        'description'
    ];

    /**
     * Video types
     * @var array
     */
    public static $types = [
        'other' => 0,
        'city' => 1,
        'country' => 2,
        'logo' => 3,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function videoCity()
    {
        return $this->hasOne(City::class, 'code', 'city');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function videoCountry()
    {
        return $this->hasOne(Country::class, 'code', 'country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
       return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Get Videos
     * @return mixed
     */
    public function getVideos()
    {
        return $this->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Store Videos
     * @param $videos
     * @return mixed
     */
    public function storeVideos($videos)
    {
        return $this->insert($videos);
    }

    /**
     * Update Video
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateVideo($id, $data)
    {
        return $this->find($id)->update($data);
    }

}
