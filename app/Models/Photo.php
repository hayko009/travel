<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'type',
        'city',
        'country',
        'locale',
        'path',
        'description'
    ];

    /**
     * Photo types
     * @var array
     */
    public static $types = [
        'other' => 0,
        'city' => 1,
        'country' => 2,
        'logo' => 3,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function photoCity()
    {
        return $this->hasOne(City::class, 'code', 'city');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function photoCountry()
    {
        return $this->hasOne(Country::class, 'code', 'country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
       return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Get Photos
     * @return mixed
     */
    public function getPhotos()
    {
        return $this->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Store Photos
     * @param $photos
     * @return mixed
     */
    public function storePhotos($photos)
    {
        return $this->insert($photos);
    }

    /**
     * Update Photo
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updatePhoto($id, $data)
    {
        return $this->find($id)->update($data);
    }

}
