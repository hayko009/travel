<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * Class Airport
 * @package App\Models
 */
class Airport extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'code',
        'ordering',
        'name',
        'params'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'params'  => 'array',
        'name'  => 'array',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'name',
        'cases',
    ];

    /**
     * Get Attribute Name
     * @param $name
     * @return mixed
     */
    public function getNameAttribute($name)
    {
        $name = json_decode($name, true);

        if (!empty($name[App::getLocale()])) {
            return $name[App::getLocale()];
        }

        return $name[env('DEFAULT_LANGUAGE')];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
