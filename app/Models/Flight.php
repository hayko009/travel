<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Flight
 * @package App\Models
 */
class Flight extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'from_id',
        'to_id',
        'ordering',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function from()
    {
        return $this->hasOne(City::class, 'id', 'from_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function to()
    {
        return $this->hasOne(City::class, 'id', 'to_id');
    }
}
