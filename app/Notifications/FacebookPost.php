<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\FacebookPoster\FacebookPosterChannel;
use NotificationChannels\FacebookPoster\FacebookPosterPost;

class FacebookPost extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FacebookPosterChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toFacebookPoster($notifiable)
    {
        $notifiable = $notifiable->routes['data'];

        $postFacebook = (new FacebookPosterPost($notifiable['content']));

        if (!empty($notifiable['link'])) {
            $postFacebook = $postFacebook
                ->withLink($notifiable['link']);
                //->scheduledFor($notifiable['scheduledFor']);
        }

        if (!empty($notifiable['img'])) {
            $postFacebook = $postFacebook
                ->withImage($notifiable['img']);
                //->scheduledFor($notifiable['scheduledFor']);
        }

        if (!empty($notifiable['video']) && !empty($notifiable['video']['url'])) {
            $postFacebook = $postFacebook
                ->withVideo($notifiable['video']['url'], $notifiable['video']);
                //->scheduledFor($notifiable['scheduledFor']);
        }

        return $postFacebook;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
