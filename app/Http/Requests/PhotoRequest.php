<?php

namespace App\Http\Requests;

use App\Models\Photo;
use Illuminate\Foundation\Http\FormRequest;

class PhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $photoTypes = array_values(Photo::$types);

        $rules = [
            'type' => 'required|in:'.implode(',', $photoTypes),
            'city' => '',
            'country' => '',
            'description' => '',
            'photos' => 'required',
            'photos.*' => 'required|mimetypes:image/jpeg,image/jpg,image/png,image/gif'
        ];

        if ($this->has('type')
            && in_array($this->get('type'), $photoTypes)
            && $this->get('type') == Photo::$types['city']
        ) {
            $rules['city'] = 'required|string';
        }

        if ($this->has('type')
            && in_array($this->get('type'), $photoTypes)
            && $this->get('type') == Photo::$types['country']
        ) {
            $rules['country'] = 'required|string';
        }

        if ($this->isMethod('put')) {
            unset($rules['type']);
            unset($rules['city']);
            unset($rules['country']);
            unset($rules['photos']);
            unset($rules['photos.*']);
        }

        return $rules;
    }
}
