<?php

namespace App\Http\Requests;

use App\Models\Video;
use Illuminate\Foundation\Http\FormRequest;

class VideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $videoTypes = array_values(Video::$types);

        $rules = [
            'type' => 'required|in:'.implode(',', $videoTypes),
            'city' => '',
            'country' => '',
            'description' => '',
            'youtube' => '',
            'vimeo' => '',
            'videos' => 'required',
            'videos.*' => 'required|mimetypes:video/webm,video/mp4,audio/mpeg'
        ];

        if ($this->has('type')
            && in_array($this->get('type'), $videoTypes)
            && $this->get('type') == Video::$types['city']
        ) {
            $rules['city'] = 'required|string';
        }

        if ($this->has('type')
            && in_array($this->get('type'), $videoTypes)
            && $this->get('type') == Video::$types['country']
        ) {
            $rules['country'] = 'required|string';
        }

        if ($this->isMethod('put')) {
            unset($rules['type']);
            unset($rules['city']);
            unset($rules['country']);
            unset($rules['videos']);
            unset($rules['videos.*']);
        }

        return $rules;
    }
}
