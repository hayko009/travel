<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Country;
use App\Models\SiteSetting;
use Illuminate\Support\Facades\Request;

/**
 * Class CountryController
 * @package App\Http\Controllers
 */
class CountryController extends Controller
{
    /**
     * List Countries
     *
     * @param int $page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($page = 1)
    {
        $limit = env('COUNTRIES_LIMIT', 15);
        $pageForPagination = max($page, 1);
        $skip = max($page - 1, 0) * $limit;
        $countries =  Country::skip($skip)->take($limit)->get();
        if (!$countries->first()) {
            abort(404);
        }

        $data = [
            'meta'     => $this->getMetaData(null, $pageForPagination),
            'pagination' => Helper::pagination($pageForPagination,'page', SiteSetting::countriesCount(), $limit),
            'countries' => $countries];

        // Generate Random Links
        $data = array_merge(Helper::generateRandomLinks(true), $data);

        return view(config('template').'.country.index', $data);
    }

    /**
     * Single Country
     *
     * @param $code
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($code)
    {
        if ($country = Country::where('code', $code)->first()) {
            $data = [
                'meta'            => $this->getMetaData($country),
                'country'         => $country,
                'randomCountries' => Helper::getRandomCountries($code)
            ];

            // Generate Random Links
            $data = array_merge(Helper::generateRandomLinks(true, $code), $data);

            return view(config('template').'.country.show', $data);
        }

        return abort(404);
    }

    /**
     * Get Meta Data
     *
     * @param \App\Models\Country|null $country
     * @param $page
     *
     * @return array
     */
    protected function getMetaData(Country $country = null, $page = null)
    {
        if ($country) {
            return [
                'title'       => trans('messages.countries.metaShowTitle', ['name' => $country->name, 'code' => $country->code, 'siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.countries.metaShowKeywords', ['name' => $country->name, 'code' => $country->code, 'siteName' => env('SITE_NAME')]),
                'description' => trans('messages.countries.metaShowDescription', ['name' => $country->name, 'code' => $country->code, 'siteName' => env('SITE_NAME')]),
            ];
        } elseif (!empty(Request::segment(3))) {
            return [
                'title'       => trans('messages.countries.metaPageTitle', ['page' => $page,'siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.countries.metaPageKeywords', ['page' => $page,'siteName' => env('SITE_NAME')]),
                'description' => trans('messages.countries.metaPageDescription', ['page' => $page,'siteName' => env('SITE_NAME')]),
            ];
        } else {
            return [
                'title'       => trans('messages.countries.metaListTitle', ['siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.countries.metaListKeywords', ['siteName' => env('SITE_NAME')]),
                'description' => trans('messages.countries.metaListDescription', ['siteName' => env('SITE_NAME')]),
            ];
        }
    }
}
