<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\City;
use App\Models\SiteSetting;
use Illuminate\Support\Facades\Request;

/**
 * Class FlightController
 * @package App\Http\Controllers
 */
class FlightController extends Controller
{

    /**
     * Search Flights
     *
     * @param null $from
     * @param null $to
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($from = null, $to = null)
    {
        if ($from && $to) {
            if (is_numeric($to)) {
                if($from = City::orderBy('id', 'desc')->whereNotNull('img')->where('code', $from)->first()) {
                    return $this->lists($from, $to);
                }
            } elseif ($to = City::where('code', $to)->first()) {
                $from = City::where('code', $from)->first();
                return $this->show($from, $to);
            }
        } elseif (!isset($from) && !isset($to)) {
            $from = City::orderBy('id', 'desc')->whereNotNull('img')->first();
            return $this->lists($from, 1);
        }

        return abort(404);
    }

    /**
     * Flights Show Page
     *
     * @param \App\Models\City $from
     * @param \App\Models\City $to
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(City $from, City $to)
    {
        $data = [
            'meta' => $this->getMetaData($from, $to),
            'from' => $from,
            'to'   => $to
        ];

        // Get Random Flights
        $data = array_merge($data, Helper::getRandomFlights($from, $to));

        // Generate Random Links
        $data = array_merge(Helper::generateRandomLinks(true, null, null, null, null, $from, $to), $data);

        return view(config('template').'.flight.show', $data);
    }

    /**
     * List full Cities By from code
     *
     * @param \App\Models\City $from
     * @param                  $page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lists(City $from, $page)
    {
        $limit = env('FULLCITIES_LIMIT', 15);
        $pageForPagination = max($page, 1);
        $skip = max($page - 1, 0) * $limit;
        $flights =  City::orderBy('id', 'desc')->whereNotNull('img')->skip($skip)->take($limit)->get();
        if (!$flights->first()) {
            abort(404);
        }

        $data = [
            'flights' => $flights,
            'pagination' => Helper::pagination($pageForPagination, $from, SiteSetting::fullCitiesCount(), $limit, ['img' => 'notNull']),
            'meta'    => $this->getMetaData($from, null, $pageForPagination),
            'from'    => $from
        ];

        // Generate Random Links
        $data = array_merge(Helper::generateRandomLinks(true), $data);

        return view(config('template').'.flight.index', $data);
    }

    /**
     * Get Meta Data
     *
     * @param \App\Models\City      $from
     * @param \App\Models\City|null $to
     * @param null                  $page
     *
     * @return array
     */
    protected function getMetaData(City $from, City $to = null, $page = null)
    {
        if ($from && $to) {
            $dataForReplace = [
                'from'     => $from->name,
                'to'       => $to->name,
                'siteName' => env('SITE_NAME')
            ];

            return [
                'title'       => trans('messages.flights.metaShowTitle', $dataForReplace),
                'keywords'    => trans('messages.flights.metaShowKeywords', $dataForReplace),
                'description' => trans('messages.flights.metaShowDescription', $dataForReplace),
            ];
        } elseif(!empty(Request::segment(3))) {
            return [
                'title'       => trans('messages.flights.metaPageTitle', ['page' => $page, 'fromName' => $from->name]),
                'keywords'    => trans('messages.flights.metaPageKeywords', ['page' => $page, 'fromName' => $from->name]),
                'description' => trans('messages.flights.metaPageDescription', ['page' => $page, 'fromName' => $from->name]),
            ];
        } else {
            return [
                'title'       => trans('messages.flights.metaListTitle', ['siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.flights.metaListKeywords', ['siteName' => env('SITE_NAME')]),
                'description' => trans('messages.flights.metaListDescription', ['siteName' => env('SITE_NAME')]),
            ];
        }
    }
}
