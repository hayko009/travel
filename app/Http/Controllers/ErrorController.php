<?php

namespace App\Http\Controllers;

/**
 * Class ErrorController
 * @package App\Http\Controllers
 */
class ErrorController extends Controller
{
    /**
     * not-found Page not found
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notfound()
    {
        return view(config('template').'.errors.not-found');
    }
}
