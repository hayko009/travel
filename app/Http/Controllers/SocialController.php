<?php

namespace App\Http\Controllers;

use App\Models\Airline;
use App\Models\Social;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Models\City;
use Illuminate\Notifications\Notifiable;
use App\Notifications\FacebookPost;
use Illuminate\Support\Facades\Notification;

class SocialController extends Controller
{
    public function postVideo()
    {

        $postedVideos = Social::where('type', Social::$types['video'])
            ->where('social', Social::$socials['fb'])
            ->pluck('type_id')
            ->toArray();

        $video = Video::whereNotIn('id', $postedVideos)->inRandomOrder()->first();

        if ($video) {
            $videoPath = '';
            $modelName = '';
            switch ($video->type) {
                case Video::$types['city']:
                    $videoPath = 'videos/cities';
                    $modelName = 'city';
                    break;
                case Video::$types['country']:
                    $videoPath = 'videos/countries';
                    $modelName = 'country';
                    break;
                case Video::$types['other']:
                    $videoPath = 'videos/others';
                    $modelName = '';
                    break;
                case Video::$types['logo']:
                    $videoPath = 'videos/logos';
                    $modelName = '';
                    break;
            }

            // todo get model data
            $data = [
                'content' => 'Video test Content',
                'video' => [
                    'url' => url('/'.$videoPath.'/'.$video->path),
                    'title' => 'Test video title',
                    'description' => 'Test video description',
                ],
                'scheduledFor' => strtotime("+20 minutes")
            ];

            //exec('ffmpeg -i public/'.$videoPath.'/'.$video->path.' -i public/images/logo.png -strict -2  -filter_complex "overlay=x=W-w-10:y=H-h-10" public/'.$videoPath.'/video-with-logo.mp4');
            //ffmpeg -i input1.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate1.ts
            //ffmpeg -i input2.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate2.ts
            //ffmpeg -i "concat:intermediate1.ts|intermediate2.ts" -c copy -bsf:a aac_adtstoasc output.mp4

            // Facebook poster
            //Notification::route('data', $data)->notify(new FacebookPost($data));

            Social::create([
                'type_id' => $video->id,
                'social' => Social::$socials['fb'],
                'type' => Social::$types['video']
            ]);
        }
    }

    /**
     * Post City img
     */
    public function postImage()
    {
        $city = City::whereNotNull('img')->inRandomOrder()->first();
        $data = [
            'content' => trans('messages.postsSocials.facebook.city', [
                'country' => $city->country->name,
                'city' => $city->name,
                'countryHash' => str_replace(' ', '', ucwords($city->country->name)),
                'cityHash' => str_replace(' ', '', ucwords($city->name)),
                'siteName' => env('SITE_NAME'),
                'siteUrl' => url('/'),
                'cityUrl' => url('/cities/'.$city->code),
            ]),
            'img' => url('/images/cities/'.$city->img),
            'video' => '',
            'link' => '',
        ];
        Notification::route('data', $data)->notify(new FacebookPost($data));
    }
}
