<?php

namespace App\Http\Controllers;


use App\Helpers\Helper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'meta' => [
                'title'       => trans('messages.metaTitle'),
                'keywords'    => trans('messages.metaKeywords'),
                'description' => trans('messages.metaDescription'),
            ]
        ];

        // Generate Random Links
        $data = array_merge(Helper::generateRandomLinks(true), $data);

        return view(config('template').'.home', $data);
    }

    /**
     * Search Flights
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        $data = [
            'meta' => [
                'title'       => trans('messages.search.metaTitle'),
                'keywords'    => trans('messages.search.metaKeywords'),
                'description' => trans('messages.search.metaDescription'),
            ]
        ];

        // Generate Random Links
        $data = array_merge(Helper::generateRandomLinks(true), $data);

        return view(config('template').'.search', $data);
    }
}
