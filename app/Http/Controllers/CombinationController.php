<?php

namespace App\Http\Controllers;

use App\Helpers\Factory;
use Illuminate\Support\Facades\Redirect;
use App\Models\SiteSetting;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Request;

class CombinationController extends Controller
{
    /**
     * From Model
     * @var bool
     */
    private $from = false;

    /**
     * To model
     * @var bool
     */
    private $to = false;

    /**
     * From count
     * @var int
     */
    private $fromCount = 0;

    /**
     * To Count
     * @var int
     */
    private $toCount = 0;

    /**
     * Trans key for translate
     * @var string
     */
    private $transKey = '';

    /**
     * View folder name
     * @var string
     */
    private $viewFolderName = '';


    /**
     * CombinationController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->setConfigs($request);
    }

    /**
     * Search Flights
     *
     * @param null $from
     * @param null $to
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($from = null, $to = null)
    {
        if ($from && $to) {
            $from = $this->from::orderBy('id', 'desc')->where('code', $from)->first();
            if ($from && is_numeric($to)) {
                return $this->lists($from, $to);
            } elseif ($from && $to = $this->to::where('code', $to)->first()) {
                return $this->show($from, $to);
            }
        } elseif (!isset($from) && !isset($to)) {
            $from = $this->from::orderBy('id', 'desc')->first();
            return $this->lists($from, 1);
        }

        return abort(404);
    }

    /**
     * Flights Show Page
     * @param $from
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($from, $to)
    {
        $data = [
            'meta' => $this->getMetaData($from, $to),
            'from' => $from,
            'to'   => $to
        ];

        // Get Random Flights
        $data = array_merge($data, Helper::getRandomFlights($from, $to));

        // Generate Random Links
        $data = array_merge(Helper::generateRandomLinks(true, null, null, null, null, $from, $to), $data);

        return view(config('template').'.'.$this->viewFolderName.'.show', $data);
    }

    /**
     * Combination list By from code
     * @param $from
     * @param $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lists($from, $page)
    {
        $limit = env('COMBINATIONS_LIMIT', 15);
        $pageForPagination = max($page, 1);
        $skip = max($page - 1, 0) * $limit;

        $flights =  $this->to::orderBy('id', 'desc')->skip($skip)->take($limit)->get();
        if (!$flights->first()) {
            abort(404);
        }

        $data = [
            'flights' => $flights,
            'pagination' => Helper::pagination($pageForPagination, $from, $this->toCount, $limit),
            'meta'    => $this->getMetaData($from, null, $pageForPagination),
            'from'    => $from
        ];

        // Generate Random Links
        $data = array_merge(Helper::generateRandomLinks(true), $data);
        return view(config('template').'.'.$this->viewFolderName.'.index', $data);
    }

    /**
     * Get Meta Data
     * @param $from
     * @param null $to
     * @param null $page
     * @return array
     */
    protected function getMetaData($from, $to = null, $page = null)
    {
        if ($from && $to) {
            $dataForReplace = [
                'from'     => $from->name,
                'to'       => $to->name,
                'fromCode' => $from->code,
                'toCode'   => $to->code
            ];

            return [
                'title'       => trans('messages.'.$this->transKey.'.metaShowTitle', $dataForReplace),
                'keywords'    => trans('messages.'.$this->transKey.'.metaShowKeywords', $dataForReplace),
                'description' => trans('messages.'.$this->transKey.'.metaShowDescription', $dataForReplace),
            ];
        } elseif(!empty(Request::segment(3))) {
            return [
                'title'       => trans('messages.'.$this->transKey.'.metaPageTitle', ['page' => $page, 'fromName' => $from->name]),
                'keywords'    => trans('messages.'.$this->transKey.'.metaPageKeywords', ['page' => $page, 'fromName' => $from->name]),
                'description' => trans('messages.'.$this->transKey.'.metaPageDescription', ['page' => $page, 'fromName' => $from->name]),
            ];
        } else {
            return [
                'title'       => trans('messages.'.$this->transKey.'.metaListTitle'),
                'keywords'    => trans('messages.'.$this->transKey.'.metaListKeywords'),
                'description' => trans('messages.'.$this->transKey.'.metaListDescription'),
            ];
        }
    }

    /**
     * Set Combination config
     * @return \Illuminate\Http\RedirectResponse
     */
    private function setConfigs()
    {
        try {
            $segment = Request::segment(1);
            $segemntArray = explode('-to-', $segment);
            if (count($segemntArray) == 2)
            {
                $from = $segemntArray[0];
                $to = $segemntArray[1];
                $this->from = Factory::get("App\\Models\\".ucfirst($from));
                $this->to = Factory::get("App\\Models\\".ucfirst($to));
                $fromCountFunctionName = str_plural($from) .'Count';
                $toCountFunctionName = str_plural($to) .'Count';
                $this->fromCount = SiteSetting::{$fromCountFunctionName}();
                $this->toCount = SiteSetting::{$toCountFunctionName}();
                $this->transKey = $from.'To'.ucfirst($to);
                $this->viewFolderName = $segment;
            }
        } catch (\Exception $exception) {
            return abort(404);
        }
    }


}
