<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\Country;
use App\Models\Photo;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Requests\PhotoRequest;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

/**
 * Class PhotoController
 * @package App\Http\Controllers\Admin
 */
class PhotoController extends Controller
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * @var \App\Models\Photo
     */
    protected $photo;

    /**
     * PhotoController constructor.
     *
     * @param \Illuminate\Contracts\Auth\Guard $auth
     * @param \App\Models\Photo                $photo
     */
    public function __construct(Guard $auth, Photo $photo)
    {
        $this->middleware('auth');

        $this->auth = $auth;
        $this->photo = $photo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'photos' => $this->photo->getPhotos(),
            'types' => Photo::$types
        ];
        return view('admin.photo.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'photoTypes' => array_map('ucfirst', array_flip(Photo::$types)),
            'cities' => City::get()->pluck('name', 'code')->toArray(),
            'countries' => Country::get()->pluck('name', 'code')->toArray(),
        ];
        return view('admin.photo.create', $data);
    }

    /**
     * Store photo
     * @param \App\Http\Requests\PhotoRequest $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(PhotoRequest $request)
    {
        $photos = $this->buildPhotos($request);
        if(count($photos) && $this->photo->storePhotos($photos)) {
            return redirect('trip-admin/photo')->with('success', 'Photos has been successfully uploaded!');
        } else {
            return redirect('trip-admin/photo')->withErrors('Upload problem');
        }
    }

//    /**
//     * Display the specified resource.
//     *
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
//    public function show($id)
//    {
//        if (null != $category = $this->categoryService->getCategoryByID( $id )) {
//            return view('admin.categories.show', ['category' => $category]);
//        }
//
//        return redirect('admin/category');
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (null != $photo = $this->photo->find( $id )) {
            $data = [
                'photoTypes' => array_map('ucfirst', array_flip(Photo::$types)),
                'cities' => City::get()->pluck('name', 'code')->toArray(),
                'countries' => Country::get()->pluck('name', 'code')->toArray(),
                'photo' => $photo
            ];
            return view('admin.photo.edit', $data);
        }

        return redirect('/trip-admin/photo');
    }

    /**
     * Update the specified resource in storage.
     * @param PhotoRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PhotoRequest $request, $id)
    {
        if($this->photo->updatePhoto( $id , $request->all())) {
            return redirect('trip-admin/photo')->with('success', 'Photo has been successfully updated!');
        }

        return redirect('trip-admin/photo')->withErrors('Update problem');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($photos = $this->photo->find( $id )) {
            switch ($photos->type) {
                case Photo::$types['city']:
                    $folderName = 'cities';
                    break;
                case Photo::$types['country']:
                    $folderName = 'countries';
                    break;
                case Photo::$types['logo']:
                    $folderName = 'logos';
                    break;
                case Photo::$types['other']:
                    $folderName = 'others';
                    break;
                default:
                    $folderName = 'others';
                    break;
            }

            if(File::exists('photos/'.$folderName.'/'.$photos->path)) {
                File::delete('photos/'.$folderName.'/'.$photos->path);
            }

            $photos->delete();

            return redirect('/trip-admin/photo')->with('success', 'Photo has been successfully deleted!');
        } else {
            return redirect('trip-admin/photo')->withErrors('Delete Problem');
        }
    }

    /**
     * Build and upload photos
     * @param $request
     * @return array
     */
    private function buildPhotos($request)
    {
        $photos = [];

        switch ($request->get('type')) {
            case Photo::$types['city']:
                $folderName = 'cities';
                $pathPrefix = $request->get('city');
                break;
            case Photo::$types['country']:
                $folderName = 'countries';
                $pathPrefix = $request->get('country');
                break;
            case Photo::$types['logo']:
                $folderName = 'logos';
                $pathPrefix = 'logo';
                break;
            case Photo::$types['other']:
                $folderName = 'others';
                $pathPrefix = 'other';
                break;
            default:
                $folderName = 'others';
                $pathPrefix = 'other';
                break;
        }

        foreach ($request->file('photos') as $photo) {
            $fileName = $pathPrefix . '_' . time() . '.' . $photo->getClientOriginalExtension();
            $photo->move(base_path() . '/public/photos/'.$folderName, $fileName);
            $photos[] = [
                'type' => $request->get('type'),
                'city' => $request->get('city')?? null,
                'country' => $request->get('country')?? null,
                'locale' => App::getLocale(),
                'description' => $request->get('description'),
                'path' => $fileName,
                'user_id' => Auth::id(),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ];
        }

        return $photos;
    }
}
