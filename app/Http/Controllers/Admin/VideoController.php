<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\Country;
use App\Models\Video;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Requests\VideoRequest;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

/**
 * Class VideoController
 * @package App\Http\Controllers\Admin
 */
class VideoController extends Controller
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * @var \App\Models\Video
     */
    protected $video;

    /**
     * VideoController constructor.
     *
     * @param \Illuminate\Contracts\Auth\Guard $auth
     * @param \App\Models\Video                $video
     */
    public function __construct(Guard $auth, Video $video)
    {
        $this->middleware('auth');

        $this->auth = $auth;
        $this->video = $video;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'videos' => $this->video->getVideos(),
            'types' => Video::$types
        ];
        return view('admin.video.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'videoTypes' => array_map('ucfirst', array_flip(Video::$types)),
            'cities' => City::get()->pluck('name', 'code')->toArray(),
            'countries' => Country::get()->pluck('name', 'code')->toArray(),
        ];
        return view('admin.video.create', $data);
    }

    /**
     * Store video
     * @param \App\Http\Requests\VideoRequest $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(VideoRequest $request)
    {
        $videos = $this->buildVideos($request);
        if(count($videos) && $this->video->storeVideos($videos)) {
            return redirect('trip-admin/video')->with('success', 'Videos has been successfully uploaded!');
        } else {
            return redirect('trip-admin/video')->withErrors('Upload problem');
        }
    }

//    /**
//     * Display the specified resource.
//     *
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
//    public function show($id)
//    {
//        if (null != $category = $this->categoryService->getCategoryByID( $id )) {
//            return view('admin.categories.show', ['category' => $category]);
//        }
//
//        return redirect('admin/category');
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (null != $video = $this->video->find( $id )) {
            $data = [
                'videoTypes' => array_map('ucfirst', array_flip(Video::$types)),
                'cities' => City::get()->pluck('name', 'code')->toArray(),
                'countries' => Country::get()->pluck('name', 'code')->toArray(),
                'video' => $video
            ];
            return view('admin.video.edit', $data);
        }

        return redirect('/trip-admin/video');
    }

    /**
     * Update the specified resource in storage.
     * @param VideoRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(VideoRequest $request, $id)
    {
        if($this->video->updateVideo( $id , $request->all())) {
            return redirect('trip-admin/video')->with('success', 'Video has been successfully updated!');
        }

        return redirect('trip-admin/video')->withErrors('Update problem');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($video = $this->video->find( $id )) {
            switch ($video->type) {
                case Video::$types['city']:
                    $folderName = 'cities';
                    break;
                case Video::$types['country']:
                    $folderName = 'countries';
                    break;
                case Video::$types['logo']:
                    $folderName = 'logos';
                    break;
                case Video::$types['other']:
                    $folderName = 'others';
                    break;
                default:
                    $folderName = 'others';
                    break;
            }

            if(File::exists('videos/'.$folderName.'/'.$video->path)) {
                File::delete('videos/'.$folderName.'/'.$video->path);
            }

            $video->delete();

            return redirect('/trip-admin/video')->with('success', 'Video has been successfully deleted!');
        } else {
            return redirect('trip-admin/video')->withErrors('Delete Problem');
        }
    }

    /**
     * Build and upload videos
     * @param $request
     * @return array
     */
    private function buildVideos($request)
    {
        $videos = [];

        switch ($request->get('type')) {
            case Video::$types['city']:
                $folderName = 'cities';
                $pathPrefix = $request->get('city');
                break;
            case Video::$types['country']:
                $folderName = 'countries';
                $pathPrefix = $request->get('country');
                break;
            case Video::$types['logo']:
                $folderName = 'logos';
                $pathPrefix = 'logo';
                break;
            case Video::$types['other']:
                $folderName = 'others';
                $pathPrefix = 'other';
                break;
            default:
                $folderName = 'others';
                $pathPrefix = 'other';
                break;
        }

        foreach ($request->file('videos') as $video) {
            $fileName = $pathPrefix . '_' . time() . '.' . $video->getClientOriginalExtension();
            $video->move(base_path() . '/public/videos/'.$folderName, $fileName);
            $videos[] = [
                'type' => $request->get('type'),
                'city' => $request->get('city')?? null,
                'country' => $request->get('country')?? null,
                'locale' => App::getLocale(),
                'description' => $request->get('description'),
                'youtube' => $request->get('youtube'),
                'vimeo' => $request->get('vimeo'),
                'path' => $fileName,
                'user_id' => Auth::id(),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ];
        }

        return $videos;
    }
}
