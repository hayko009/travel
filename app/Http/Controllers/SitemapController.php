<?php

namespace App\Http\Controllers;

use App\Models\Airline;
use App\Models\Flight;
use App\Models\City;
use App\Models\Country;
use App\Models\Airport;
use App\Models\SiteSetting;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    /**
     * All Sitemaps index list
     * @return $this
     */
    public function index()
    {
        $cities = City::all();
        return response()->view('sitemap.index', [
            'cities' => $cities
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Flights list
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function flights(Request $request)
    {
        $cities = City::get();
        $city = false;
        $sitemapIndex = true;
        $count = SiteSetting::fullCitiesCount();
        if ($request->has('city')) {
            $city = City::find($request->input('city', 1));
            $sitemapIndex = false;
        }

        return response()->view('sitemap.flights', [
            'city' => $city,
            'cities' => $cities,
            'count' => $count,
            'sitemapIndex' => $sitemapIndex
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * City to city list
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cityToCity(Request $request)
    {
        $cities = City::get();
        $city = false;
        $sitemapIndex = true;
        $count = SiteSetting::citiesCount();
        if ($request->has('city')) {
            $city = City::find($request->input('city', 1));
            $sitemapIndex = false;
        }

        return response()->view('sitemap.city-to-city', [
            'city' => $city,
            'cities' => $cities,
            'count' => $count,
            'sitemapIndex' => $sitemapIndex
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * City to country list
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cityToCountry(Request $request)
    {
        $cities = City::get();
        $countries = Country::get();
        $city = false;
        $sitemapIndex = true;
        $count = SiteSetting::countriesCount();
        if ($request->has('city')) {
            $city = City::find($request->input('city', 1));
            $sitemapIndex = false;
        }

        return response()->view('sitemap.city-to-country', [
            'city' => $city,
            'cities' => $cities,
            'countries' => $countries,
            'count' => $count,
            'sitemapIndex' => $sitemapIndex
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * City to airport list
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cityToAirport(Request $request)
    {
        $cities = City::get();
        $airports = Airport::get();
        $city = false;
        $sitemapIndex = true;
        $count = SiteSetting::airportsCount();
        if ($request->has('city')) {
            $city = City::find($request->input('city', 1));
            $sitemapIndex = false;
        }

        return response()->view('sitemap.city-to-airport', [
            'city' => $city,
            'cities' => $cities,
            'airports' => $airports,
            'count' => $count,
            'sitemapIndex' => $sitemapIndex
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Country to country list
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function countryToCountry(Request $request)
    {
        $countries = Country::get();
        $country = false;
        $sitemapIndex = true;
        $count = SiteSetting::countriesCount();
        if ($request->has('country')) {
            $country = Country::find($request->input('country', 1));
            $sitemapIndex = false;
        }

        return response()->view('sitemap.country-to-country', [
            'country' => $country,
            'countries' => $countries,
            'count' => $count,
            'sitemapIndex' => $sitemapIndex
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Country to city list
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function countryToCity(Request $request)
    {
        $countries = Country::get();
        $cities = City::get();
        $country = false;
        $sitemapIndex = true;
        $count = SiteSetting::citiesCount();
        if ($request->has('country')) {
            $country = Country::find($request->input('country', 1));
            $sitemapIndex = false;
        }

        return response()->view('sitemap.country-to-city', [
            'country' => $country,
            'countries' => $countries,
            'cities' => $cities,
            'count' => $count,
            'sitemapIndex' => $sitemapIndex
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Country to airport list
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function countryToAirport(Request $request)
    {
        $countries = Country::get();
        $airports = Airport::get();
        $country = false;
        $sitemapIndex = true;
        $count = SiteSetting::airportsCount();
        if ($request->has('country')) {
            $country = Country::find($request->input('country', 1));
            $sitemapIndex = false;
        }

        return response()->view('sitemap.country-to-airport', [
            'country' => $country,
            'countries' => $countries,
            'airports' => $airports,
            'count' => $count,
            'sitemapIndex' => $sitemapIndex
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Airport to airport list
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function airportToAirport(Request $request)
    {
        $airports = Airport::get();
        $airport = false;
        $sitemapIndex = true;
        $count = SiteSetting::airportsCount();
        if ($request->has('airport')) {
            $airport = Airport::find($request->input('airport', 1));
            $sitemapIndex = false;
        }

        return response()->view('sitemap.airport-to-airport', [
            'airport' => $airport,
            'airports' => $airports,
            'count' => $count,
            'sitemapIndex' => $sitemapIndex
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Airport to city list
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function airportToCity(Request $request)
    {
        $airports = Airport::get();
        $cities = City::get();
        $airport = false;
        $sitemapIndex = true;
        $count = SiteSetting::citiesCount();
        if ($request->has('airport')) {
            $airport = Airport::find($request->input('airport', 1));
            $sitemapIndex = false;
        }

        return response()->view('sitemap.airport-to-city', [
            'airport' => $airport,
            'airports' => $airports,
            'cities' => $cities,
            'count' => $count,
            'sitemapIndex' => $sitemapIndex
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Airport to country list
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function airportToCountry(Request $request)
    {
        $airports = Airport::get();
        $countries = Country::get();
        $airport = false;
        $sitemapIndex = true;
        $count = SiteSetting::countriesCount();
        if ($request->has('airport')) {
            $airport = Airport::find($request->input('airport', 1));
            $sitemapIndex = false;
        }

        return response()->view('sitemap.airport-to-country', [
            'airport' => $airport,
            'airports' => $airports,
            'countries' => $countries,
            'count' => $count,
            'sitemapIndex' => $sitemapIndex
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Airlines
     * @return \Illuminate\Http\Response
     */
    public function airlines()
    {
        $airlines = Airline::all();
        return response()->view('sitemap.airlines', [
            'airlines' => $airlines
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Countries
     * @return \Illuminate\Http\Response
     */
    public function countries()
    {
        $countries = Country::all();
        return response()->view('sitemap.countries', [
            'countries' => $countries
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Cities
     * @return \Illuminate\Http\Response
     */
    public function cities()
    {
        $cities = City::all();
        return response()->view('sitemap.cities', [
            'cities' => $cities
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Airports
     * @return \Illuminate\Http\Response
     */
    public function airports()
    {
        $airports = Airport::all();
        return response()->view('sitemap.airports', [
            'airports' => $airports
        ])->header('Content-Type', 'text/xml');
    }

    /**
     * Pages
     * @return \Illuminate\Http\Response
     */
    public function pages()
    {
        return response()->view('sitemap.pages', [])->header('Content-Type', 'text/xml');
    }

    public function citiesImages()
    {
        $cities = City::whereNotNull('img')->get();
        return response()->view('sitemap.cities-images', [
            'cities' => $cities
        ])->header('Content-Type', 'text/xml');
    }
}
