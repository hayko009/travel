<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Airport;
use App\Models\SiteSetting;
use Illuminate\Support\Facades\Request;

/**
 * Class AirportController
 * @package App\Http\Controllers
 */
class AirportController extends Controller
{
    /**
     * List Airports
     *
     * @param int $page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($page = 1)
    {
        $limit = env('AIRPORTS_LIMIT', 15);
        $pageForPagination = max($page, 1);
        $skip = max($page - 1, 0) * $limit;
        $airports =  Airport::skip($skip)->take($limit)->get();
        if (!$airports->first()) {
            abort(404);
        }

        $data = [
            'meta'     => $this->getMetaData(null, $pageForPagination),
            'pagination' => Helper::pagination($pageForPagination,'page', SiteSetting::airportsCount(), $limit),
            'airports' => $airports
        ];

        // Generate Random Links
        $data = array_merge(Helper::generateRandomLinks(true), $data);

        return view(config('template').'.airport.index', $data);
    }

    /**
     * Single Airport
     *
     * @param $code
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($code)
    {
        if ($airport = Airport::where('code', $code)->first()) {
            $data = [
                'meta'           => $this->getMetaData($airport),
                'airport'        => $airport,
                'randomAirports' => Helper::getRandomAirports($code)
            ];

            // Generate Random Links
            $data = array_merge(Helper::generateRandomLinks(true, null, null, $code), $data);

            return view(config('template').'.airport.show', $data);
        }

        return abort(404);
    }

    /**
     * Get Meta Data
     *
     * @param \App\Models\Airport|null $airport
     * @param $page
     *
     * @return array
     */
    protected function getMetaData(Airport $airport = null, $page = null)
    {
        if ($airport) {
            return [
                'title'       => trans('messages.airports.metaShowTitle', ['name' => $airport->name, 'code' => $airport->code, 'siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.airports.metaShowKeywords', ['name' => $airport->name, 'code' => $airport->code, 'siteName' => env('SITE_NAME')]),
                'description' => trans('messages.airports.metaShowDescription', ['name' => $airport->name, 'code' => $airport->code, 'siteName' => env('SITE_NAME')])
            ];
        } elseif (!empty(Request::segment(3))) {
            return [
                'title'       => trans('messages.airports.metaPageTitle', ['page' => $page, 'siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.airports.metaPageKeywords', ['page' => $page, 'siteName' => env('SITE_NAME')]),
                'description' => trans('messages.airports.metaPageDescription', ['page' => $page, 'siteName' => env('SITE_NAME')]),
            ];
        } else {
            return [
                'title'       => trans('messages.airports.metaListTitle', ['siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.airports.metaListKeywords', ['siteName' => env('SITE_NAME')]),
                'description' => trans('messages.airports.metaListDescription', ['siteName' => env('SITE_NAME')]),
            ];
        }
    }
}
