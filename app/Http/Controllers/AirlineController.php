<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Airline;
use App\Models\SiteSetting;
use Illuminate\Support\Facades\Request;

/**
 * Class AirlineController
 * @package App\Http\Controllers
 */
class AirlineController extends Controller
{
    /**
     *  List Airlines
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index($page = 1)
    {
        $limit = env('AIRLINES_LIMIT', 15);
        $pageForPagination = max($page, 1);
        $skip = max($page - 1, 0) * $limit;
        $airlines =  Airline::skip($skip)->take($limit)->get();
        if (!$airlines->first()) {
            abort(404);
        }

        $data = [
            'meta'     => $this->getMetaData(null, $pageForPagination),
            'pagination' => Helper::pagination($pageForPagination,'page', SiteSetting::airlinesCount(), $limit),
            'airlines' => $airlines
        ];

        // Generate Random Links
        $data = array_merge(Helper::generateRandomLinks(true), $data);

        return view(config('template').'.airline.index', $data);
    }

    /**
     * Single Airline
     *
     * @param $code
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($code)
    {
        if ($airline = Airline::where('code', $code)->first()) {
            $data = [
                'meta'           => $this->getMetaData($airline),
                'airline'        => $airline,
                'randomAirlines' => Helper::getRandomAirlines($code)
            ];

            // Generate Random Links
            $data = array_merge(Helper::generateRandomLinks(true, null, null, null, $code), $data);

            return view(config('template').'.airline.show', $data);
        }

        return abort(404);
    }

    /**
     * Get Meta Data
     *
     * @param \App\Models\Airline|null $airline
     * @param $page
     *
     * @return array
     */
    protected function getMetaData(Airline $airline = null, $page = null)
    {
        if ($airline) {
            return [
                'title'       => trans('messages.airlines.metaShowTitle', ['name' => $airline->name, 'siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.airlines.metaShowKeywords', ['name' => $airline->name, 'siteName' => env('SITE_NAME')]),
                'description' => trans('messages.airlines.metaShowDescription', ['name' => $airline->name, 'siteName' => env('SITE_NAME')]),
            ];
        } elseif(!empty(Request::segment(3))) {
            return [
                'title'       => trans('messages.airlines.metaPageTitle', ['page' => $page, 'siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.airlines.metaPageKeywords', ['page' => $page, 'siteName' => env('SITE_NAME')]),
                'description' => trans('messages.airlines.metaPageDescription', ['page' => $page, 'siteName' => env('SITE_NAME')]),
            ];
        } else {
            return [
                'title'       => trans('messages.airlines.metaListTitle', ['siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.airlines.metaListKeywords', ['siteName' => env('SITE_NAME')]),
                'description' => trans('messages.airlines.metaListDescription', ['siteName' => env('SITE_NAME')]),
            ];
        }
    }
}
