<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\City;
use App\Models\SiteSetting;
use Illuminate\Support\Facades\Request;

/**
 * Class CityController
 * @package App\Http\Controllers
 */
class CityController extends Controller
{
    /**
     * List Cities
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index($page = 1)
    {
        $limit = env('CITIES_LIMIT', 15);
        $pageForPagination = max($page, 1);
        $skip = max($page - 1, 0) * $limit;
        $cities =  City::orderBy('img', 'desc')->skip($skip)->take($limit)->get();
        if (!$cities->first()) {
            abort(404);
        }

        $data = [
            'meta'     => $this->getMetaData(null, $pageForPagination),
            'pagination' => Helper::pagination($pageForPagination,'page', SiteSetting::citiesCount(), $limit),
            'cities' => $cities
        ];

        // Generate Random Links
        $data = array_merge(Helper::generateRandomLinks(true), $data);

        return view(config('template').'.city.index', $data);
    }

    /**
     * Single City
     *
     * @param $code
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($code)
    {
        if ($city = City::where('code', $code)->first()) {
            $data = [
                'meta'         => $this->getMetaData($city),
                'city'         => $city,
                'randomCities' => Helper::getRandomCities($code)
            ];

            // Generate Random Links
            $data = array_merge(Helper::generateRandomLinks(true, null, $code), $data);

            return view(config('template').'.city.show', $data);
        }

        return abort(404);
    }

    /**
     * Get Meta Data
     *
     * @param \App\Models\City|null $city
     * @param $page
     *
     * @return array
     */
    protected function getMetaData(City $city = null, $page = null)
    {
        if ($city) {
            return [
                'title' => trans('messages.cities.metaShowTitle', ['name' => $city->name, 'code' => $city->code, 'siteName' => env('SITE_NAME')]),
                'keywords' => trans('messages.cities.metaShowKeywords', ['name' => $city->name, 'code' => $city->code, 'siteName' => env('SITE_NAME')]),
                'description' => trans('messages.cities.metaShowDescription', ['name' => $city->name, 'code' => $city->code, 'siteName' => env('SITE_NAME')])
            ];
        } elseif (!empty(Request::segment(3))) {
            return [
                'title'       => trans('messages.cities.metaPageTitle', ['page' => $page, 'siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.cities.metaPageKeywords', ['page' => $page, 'siteName' => env('SITE_NAME')]),
                'description' => trans('messages.cities.metaPageDescription', ['page' => $page, 'siteName' => env('SITE_NAME')]),
            ];
        } else {
            return [
                'title'       => trans('messages.cities.metaListTitle', ['siteName' => env('SITE_NAME')]),
                'keywords'    => trans('messages.cities.metaListKeywords', ['siteName' => env('SITE_NAME')]),
                'description' => trans('messages.cities.metaListDescription', ['siteName' => env('SITE_NAME')])
            ];
        }
    }
}
