<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('search', 'HomeController@search');
Route::get('not-found.scss', 'ErrorController@notfound');

// Routes
Route::group(['prefix' => 'cities'], function (){
    Route::get('/', 'CityController@index');
    Route::get('{code}', 'CityController@show');
    Route::get('page/{page}', 'CityController@index');
});

Route::group(['prefix' => 'countries'], function (){
    Route::get('/', 'CountryController@index');
    Route::get('{code}', 'CountryController@show');
    Route::get('page/{page}', 'CountryController@index');
});

Route::group(['prefix' => 'airlines'], function (){
    Route::get('/', 'AirlineController@index');
    Route::get('{code}', 'AirlineController@show');
    Route::get('page/{page}', 'AirlineController@index');
});

Route::group(['prefix' => 'airports'], function (){
    Route::get('/', 'AirportController@index');
    Route::get('{code}', 'AirportController@show');
    Route::get('page/{page}', 'AirportController@index');
});

// Routes Flights

Route::group(['prefix' => 'flights'], function (){
    Route::get('/', 'FlightController@index');
    Route::get('{fromCode}/{pageOrToCode}', 'FlightController@index');
});

// Routes CombinationS From City
Route::group(['prefix' => 'city-to-city'], function (){
    Route::get('/', 'CombinationController@index');
    Route::get('{fromCode}/{pageOrToCode}', 'CombinationController@index');
});

Route::group(['prefix' => 'city-to-country'], function (){
    Route::get('/', 'CombinationController@index');
    Route::get('{fromCode}/{pageOrToCode}', 'CombinationController@index');
});

Route::group(['prefix' => 'city-to-airport'], function (){
    Route::get('/', 'CombinationController@index');
    Route::get('{fromCode}/{pageOrToCode}', 'CombinationController@index');
});

// Routes CombinationS From Country

Route::group(['prefix' => 'country-to-country'], function (){
    Route::get('/', 'CombinationController@index');
    Route::get('{fromCode}/{pageOrToCode}', 'CombinationController@index');
});

Route::group(['prefix' => 'country-to-city'], function (){
    Route::get('/', 'CombinationController@index');
    Route::get('{fromCode}/{pageOrToCode}', 'CombinationController@index');
});

Route::group(['prefix' => 'country-to-airport'], function (){
    Route::get('/', 'CombinationController@index');
    Route::get('{fromCode}/{pageOrToCode}', 'CombinationController@index');
});

// Routes CombinationS From Airport

Route::group(['prefix' => 'airport-to-country'], function (){
    Route::get('/', 'CombinationController@index');
    Route::get('{fromCode}/{pageOrToCode}', 'CombinationController@index');
});

Route::group(['prefix' => 'airport-to-city'], function (){
    Route::get('/', 'CombinationController@index');
    Route::get('{fromCode}/{pageOrToCode}', 'CombinationController@index');
});

Route::group(['prefix' => 'airport-to-airport'], function (){
    Route::get('/', 'CombinationController@index');
    Route::get('{fromCode}/{pageOrToCode}', 'CombinationController@index');
});

// /city-to-city / List all cities combinations
// /city-to-city/{code} / List all cities combinations page 1
// /city-to-city/{code}/{page} / List all cities combinations page other
// */flights/{code}/{code} / List all cities combinations single view

Route::group(['prefix' => '{robot}/sitemaps'], function (){
    Route::get('sitemap.xml','SitemapController@index');
    Route::get('airlines.xml','SitemapController@airlines');
    Route::get('countries.xml','SitemapController@countries');
    Route::get('cities.xml','SitemapController@cities');
    Route::get('airports.xml','SitemapController@airports');
    Route::get('pages.xml','SitemapController@pages');

    Route::get('flights.xml','SitemapController@flights');

    Route::get('city_to_city.xml','SitemapController@cityToCity');
    Route::get('city_to_country.xml','SitemapController@cityToCountry');
    Route::get('city_to_airport.xml','SitemapController@cityToAirport');
    Route::get('country_to_country.xml','SitemapController@countryToCountry');
    Route::get('country_to_city.xml','SitemapController@countryToCity');
    Route::get('country_to_airport.xml','SitemapController@countryToAirport');
    Route::get('airport_to_airport.xml','SitemapController@airportToAirport');
    Route::get('airport_to_city.xml','SitemapController@airportToCity');
    Route::get('airport_to_country.xml','SitemapController@airportToCountry');
    Route::get('cities_images.xml','SitemapController@citiesImages');
});

Route::group(['prefix' => 'socials'], function (){
    Route::get('post-image','SocialController@postImage');
    Route::get('post-video','SocialController@postVideo');
});

// Authentication Routes...
Route::get('trip-admin', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('trip-admin', 'Auth\LoginController@login');
Route::get('trip-admin/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset');

//Route::get('admin', function(){ return redirect('/login'); });

Route::group(['prefix' => 'trip-admin', 'middleware' => 'auth'], function (){

    Route::get('dashboard','Admin\DashboardController@index');

    Route::resource('video','Admin\VideoController');
    Route::resource('photo','Admin\PhotoController');
});

