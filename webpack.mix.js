let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.react('resources/assets/templates/tripxg/js/app.js', 'public/js/templates.tripxg.js');
mix.sass('resources/assets/templates/tripxg/sass/app.scss', 'public/css/templates.tripxg.css');
