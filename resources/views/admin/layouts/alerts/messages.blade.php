@if (!empty($errors->count()))
	<div class="alert alert-danger alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		</button>
		@foreach ($errors->all() as $key => $error)
	 		<li>{{ $error }}</li>
	 	@endforeach
	</div>
@endif
@if (Session::has('success'))
	<div class="alert alert-success alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		</button>
	 	<p>{{ Session::get('success') }}</p>
	</div>
@endif
@if (Session::has('warning'))
	<div class="alert alert-warning alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		</button>
	 	<p>{{ Session::get('warning') }}</p>
	</div>
@endif