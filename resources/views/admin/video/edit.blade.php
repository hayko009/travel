@extends('admin.layouts.layout')

@section('page_title')
	Edit video
@stop
@section('button_group')
	<div class="form-group pull-right">
		{!! Form::open( [ 'url' => url('/trip-admin/video',[$video->id]) , 'method' => 'DELETE']) !!}
			{!! Form::submit( 'Delete' , [ 'class' => 'btn btn-danger']) !!}
		{!! Form::close() !!}
	</div>
@stop

@section('content')
	<div class="row">
		@include('admin.video.forms.form')
	</div>
@stop