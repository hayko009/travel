@extends('admin.layouts.layout')

@section('page_title')
	Upload video
@stop

@section('content')
	<div class="row">
		<div class="x_panel">
		 	@include('admin.video.forms.form')
		</div>
	</div>
@stop