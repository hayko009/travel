@extends('admin.layouts.layout')

@section('page_title')
    Videos
@stop

@section('button_group')
    <div class="form-group pull-right">
        <a href="{!! url('/trip-admin/video/create') !!}" class="btn btn-info">Upload Video</a>
    </div>
@stop

@section('content')
    @if ($videos->isEmpty())
        @include('admin.layouts.alerts.noResult')
    @else
        @include('admin.video.parts.list')
    @endif
@stop