@extends('layouts.layout')

@section('page_title')
	{!! $category->title !!}
	<?php $predilections = $category->predilections; ?>
@stop


@section('content')
	    @if ($predilections->isEmpty())
	    	@include('admin.layouts.alerts.noResult')
	    @else	
	    	@include('admin.predilections.parts.list')
	    @endif
		
@stop