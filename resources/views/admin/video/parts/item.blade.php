@if($video->type == $types['city'])
	<?php $place = $video->videoCity->name.' ('.$video->videoCity->country->name.') '; ?>
@elseif($video->type == $types['country'])
    <?php $place = $video->videoCountry->name; ?>
@else
    <?php $place = 'Other'; ?>
@endif
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
	<div class="thumbnail">
		<div align="center" class="embed-responsive embed-responsive-16by9">
			<video class="video-fluid z-depth-1" loop controls muted>
				<source src="{!! \App\Helpers\Helper::generateVideoPath($video) !!}" type="video/mp4">
			</video>
		</div>
		<div class="caption">
			<p><b>Type</b> : {!! ucfirst(array_flip($types)[$video->type]) !!}</p>
			<p><b>Place</b> : <strong class="text-success">{!! $place !!}</strong></p>
			<p><b>Youtube Url</b> : <strong>{!! $video->youtube? 'Yes' : 'No' !!}</strong></p>
			<p><b>Vimeo Url</b> : <strong>{!! $video->vimeo? 'Yes' : 'No' !!}</strong></p>
			<p><b>Author</b> : <strong>{!! $video->user->name?? '-' !!}</strong></p>
			<p>
				<a href="{!! url('/trip-admin/video/'.$video->id.'/edit') !!}" class="btn btn-sm btn-info" role="button">Edit</a>
			</p>
		</div>
	</div>
</div>