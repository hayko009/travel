@if(isset($video))
    {!! Form::model($video, [ 'url' => url('trip-admin/video',[$video->id]) , 'data-parsley-validate', 'class' => 'form-horizontal form-label-left', 'method' => 'PUT', 'files' => true]) !!}
@else
    {!! Form::open( [ 'url' =>url('trip-admin/video') , 'data-parsley-validate', 'class' => 'form-horizontal form-label-left', 'method' => 'POST','files' => true ]) !!}
@endif
<div class="form-group">
    {!! Form::label('type', 'Video Type:', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12') ) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select( 'type', $videoTypes, null, [isset($video)?'disabled':'', 'title' => 'Choose one of the following...', 'class' => 'selectpicker form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('city', 'City:', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12') ) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select( 'city', $cities, null, [isset($video)?'disabled':'', 'title' => 'Choose one of the following...', 'class' => 'selectpicker form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('country', 'Country:', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12') ) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select( 'country', $countries, null, [isset($video)?'disabled':'', 'title' => 'Choose one of the following...', 'class' => 'selectpicker form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('description', 'Description:', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12') ) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::textarea( 'description', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('youtube', 'Youtube:', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12') ) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text( 'youtube', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('vimeo', 'Vimeo:', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12') ) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text( 'vimeo', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    @if(isset($video))
        <div align="center" class="embed-responsive embed-responsive-16by9">
            <video class="video-fluid z-depth-1" loop controls muted>
                <source src="{!! \App\Helpers\Helper::generateVideoPath($video) !!}" type="video/mp4">
            </video>
        </div>
    @else
        <div class="file-loading">
            <input id="file-video" class="" name="videos[]" multiple type="file" data-max-file-count ="4">
        </div>
    @endif
    <br>
</div>

<div class="form-group">
    {!! Form::submit( 'Save', ['class' => 'btn btn-success']) !!}
    <a  href="{!! url('/trip-admin/video') !!}" class="btn btn-default">Cancel</a>
</div>
{!! Form::close() !!}
