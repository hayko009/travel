@extends('admin.layouts.layout')

@section('page_title')
	Edit photo
@stop
@section('button_group')
	<div class="form-group pull-right">
		{!! Form::open( [ 'url' => url('/trip-admin/photo',[$photo->id]) , 'method' => 'DELETE']) !!}
			{!! Form::submit( 'Delete' , [ 'class' => 'btn btn-danger']) !!}
		{!! Form::close() !!}
	</div>
@stop

@section('content')
	<div class="row">
		@include('admin.photo.forms.form')
	</div>
@stop