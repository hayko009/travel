@if(isset($photo))
    {!! Form::model($photo, [ 'url' => url('trip-admin/photo',[$photo->id]) , 'data-parsley-validate', 'class' => 'form-horizontal form-label-left', 'method' => 'PUT', 'files' => true]) !!}
@else
    {!! Form::open( [ 'url' =>url('trip-admin/photo') , 'data-parsley-validate', 'class' => 'form-horizontal form-label-left', 'method' => 'POST','files' => true ]) !!}
@endif
<div class="form-group">
    {!! Form::label('type', 'Video Type:', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12') ) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select( 'type', $photoTypes, null, [isset($photo)?'disabled':'', 'title' => 'Choose one of the following...', 'class' => 'selectpicker form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('city', 'City:', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12') ) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select( 'city', $cities, null, [isset($photo)?'disabled':'', 'title' => 'Choose one of the following...', 'class' => 'selectpicker form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('country', 'Country:', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12') ) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select( 'country', $countries, null, [isset($photo)?'disabled':'', 'title' => 'Choose one of the following...', 'class' => 'selectpicker form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('description', 'Description:', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12') ) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::textarea( 'description', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    @if(isset($photo))
        <img class="img-responsive" src="{!! \App\Helpers\Helper::generatePhotoPath($photo) !!}">
    @else
        <div class="file-loading">
            <input id="file-photo" class="" name="photos[]" multiple type="file" data-max-file-count ="4">
        </div>
    @endif
    <br>
</div>

<div class="form-group">
    {!! Form::submit( 'Save', ['class' => 'btn btn-success']) !!}
    <a  href="{!! url('/trip-admin/photo') !!}" class="btn btn-default">Cancel</a>
</div>
{!! Form::close() !!}
