@if($photo->type == $types['city'])
	<?php $place = $photo->photoCity->name.' ('.$photo->photoCity->country->name.') '; ?>
@elseif($photo->type == $types['country'])
    <?php $place = $photo->photoCountry->name; ?>
@else
    <?php $place = 'Other'; ?>
@endif
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
	<div class="thumbnail">
		<div class="image view view-first">
			<img class="img-responsive" src="{!! \App\Helpers\Helper::generatePhotoPath($photo) !!}">
		</div>
		<div class="caption">
			<p><b>Type</b> : {!! ucfirst(array_flip($types)[$photo->type]) !!}</p>
			<p><b>Place</b> : <strong class="text-success">{!! $place !!}</strong></p>
			<p><b>Author</b> : <strong>{!! $photo->user->name?? '-' !!}</strong></p>
			<p>
				<a href="{!! url('/trip-admin/photo/'.$photo->id.'/edit') !!}" class="btn btn-sm btn-info" role="button">Edit</a>
			</p>
		</div>
	</div>
</div>