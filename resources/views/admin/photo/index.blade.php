@extends('admin.layouts.layout')

@section('page_title')
    Photos
@stop

@section('button_group')
    <div class="form-group pull-right">
        <a href="{!! url('/trip-admin/photo/create') !!}" class="btn btn-info">Upload Photo</a>
    </div>
@stop

@section('content')
    @if ($photos->isEmpty())
        @include('admin.layouts.alerts.noResult')
    @else
        @include('admin.photo.parts.list')
    @endif
@stop