@extends('admin.layouts.layout')

@section('page_title')
	Upload photo
@stop

@section('content')
	<div class="row">
		<div class="x_panel">
		 	@include('admin.photo.forms.form')
		</div>
	</div>
@stop