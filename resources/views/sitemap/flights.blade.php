<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

@if($sitemapIndex)
    <?php echo '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'; ?>
@else
    <?php echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'; ?>
@endif

@if($city)
    @foreach($cities as $cit)
        <url>
            <loc>{{ url('/flights/'.$city->code.'/'.$cit->code) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            @if(!empty($city->img) && !empty($cit->img))
                <priority>1.0</priority>
            @else
                <priority>0.9</priority>
            @endif
        </url>
    @endforeach
    @if(!empty($city->img))
        @for($i = 1; $i <= ceil($count/env('FULLCITIES_LIMIT', 15)); $i++)
            <url>
                <loc>{{ url('/flights/'.$city->code.'/'.$i) }}</loc>
                <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
                <changefreq>daily</changefreq>
                <priority>0.9</priority>
            </url>
        @endfor
    @endif
@else
    @foreach($cities as $cit)
        <sitemap>
            <loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/flights.xml?city='.$cit->id) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
        </sitemap>
    @endforeach
@endif

@if($sitemapIndex)
    <?php echo '</sitemapindex>'; ?>
@else
    <?php echo '</urlset>'; ?>
@endif
