<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($airports as $airport)
        <url>
            <loc>{{ url('/airports/'.$airport->code) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
    @for($i = 1; $i <= ceil(count($airports)/env('AIRPORTS_LIMIT', 15)); $i++)
        <url>
            <loc>{{ url('/airports/page/'.$i) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endfor
</urlset>