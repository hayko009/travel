<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

@if($sitemapIndex)
    <?php echo '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'; ?>
@else
    <?php echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'; ?>
@endif

@if($country)
    @foreach($countries as $countrry)
        <url>
            <loc>{{ url('/country-to-country/'.$country->code.'/'.$countrry->code) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
    @for($i = 1; $i <= ceil($count/env('COUNTRIES_LIMIT', 15)); $i++)
        <url>
            <loc>{{ url('/country-to-country/'.$country->code.'/'.$i) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.9</priority>
        </url>
    @endfor
@else
    @foreach($countries as $countrry)
        <sitemap>
            <loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/country_to_country.xml?country='.$countrry->id) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
        </sitemap>
    @endforeach
@endif

@if($sitemapIndex)
    <?php echo '</sitemapindex>'; ?>
@else
    <?php echo '</urlset>'; ?>
@endif
