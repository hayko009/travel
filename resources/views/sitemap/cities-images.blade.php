<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    @if(\Illuminate\Support\Facades\Request::segment(1) == 'yandex')
        <?php echo 'xmlns:image="https://www.yandex.ru/schemas/sitemap-image/1.1">'; ?>
    @else
        <?php echo 'xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">'; ?>
    @endif

    @foreach($cities as $city)
        <url>
            <loc>{{ url('/cities/'.$city->code) }}</loc>
            <image:image>
                <image:loc>{{ url('/images/cities/'.$city->img) }}</image:loc>
                <image:geo_location>{{ $city->country->name.', '.$city->name }}</image:geo_location>
                <image:caption>{{ trans('messages.cities.metaShowTitle', ['name' => $city->name]) }}</image:caption>
                <image:title>{{ trans('messages.cities.metaShowTitle', ['name' => $city->name]) }}</image:title>
            </image:image>
        </url>
    @endforeach
</urlset>