<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($cities as $city)
        <url>
            <loc>{{ url('/cities/'.$city->code) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            @if(!empty($city->img))
                <priority>0.9</priority>
            @else
                <priority>0.8</priority>
            @endif
        </url>
    @endforeach
    @for($i = 1; $i <= ceil(count($cities)/env('CITIES_LIMIT', 15)); $i++)
        <url>
            <loc>{{ url('/cities/page/'.$i) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endfor
</urlset>