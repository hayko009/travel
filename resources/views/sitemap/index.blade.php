<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/pages.xml') }}</loc>
        <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/cities.xml') }}</loc>
        <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/airports.xml') }}</loc>
        <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/airlines.xml') }}</loc>
        <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/countries.xml') }}</loc>
        <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/cities_images.xml') }}</loc>
        <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
    </sitemap>
    {{--<sitemap>--}}
        {{--<loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/flights.xml') }}</loc>--}}
        {{--<lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>--}}
    {{--</sitemap>--}}
    {{--<sitemap>--}}
        {{--<loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/city_to_city.xml') }}</loc>--}}
        {{--<lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>--}}
    {{--</sitemap>--}}
    {{--<sitemap>--}}
        {{--<loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/city_to_country.xml') }}</loc>--}}
        {{--<lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>--}}
    {{--</sitemap>--}}
    {{--<sitemap>--}}
        {{--<loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/city_to_airport.xml') }}</loc>--}}
        {{--<lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>--}}
    {{--</sitemap>--}}
    {{--<sitemap>--}}
        {{--<loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/country_to_country.xml') }}</loc>--}}
        {{--<lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>--}}
    {{--</sitemap>--}}
    {{--<sitemap>--}}
        {{--<loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/country_to_city.xml') }}</loc>--}}
        {{--<lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>--}}
    {{--</sitemap>--}}
    {{--<sitemap>--}}
        {{--<loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/country_to_airport.xml') }}</loc>--}}
        {{--<lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>--}}
    {{--</sitemap>--}}
    {{--<sitemap>--}}
        {{--<loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/airport_to_airport.xml') }}</loc>--}}
        {{--<lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>--}}
    {{--</sitemap>--}}
    {{--<sitemap>--}}
        {{--<loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/airport_to_city.xml') }}</loc>--}}
        {{--<lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>--}}
    {{--</sitemap>--}}
    {{--<sitemap>--}}
        {{--<loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/airport_to_country.xml') }}</loc>--}}
        {{--<lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>--}}
    {{--</sitemap>--}}
</sitemapindex>