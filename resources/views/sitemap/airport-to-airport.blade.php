<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

@if($sitemapIndex)
    <?php echo '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'; ?>
@else
    <?php echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'; ?>
@endif

@if($airport)
    @foreach($airports as $airp)
        <url>
            <loc>{{ url('/airport-to-airport/'.$airport->code.'/'.$airp->code) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
    @for($i = 1; $i <= ceil($count/env('AIRPORTS_LIMIT', 15)); $i++)
        <url>
            <loc>{{ url('/airport-to-airport/'.$airport->code.'/'.$i) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.9</priority>
        </url>
    @endfor
@else
    @foreach($airports as $airp)
        <sitemap>
            <loc>{{ url(\Illuminate\Support\Facades\Request::segment(1).'/sitemaps/airport_to_airport.xml?airport='.$airp->id) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
        </sitemap>
    @endforeach
@endif

@if($sitemapIndex)
    <?php echo '</sitemapindex>'; ?>
@else
    <?php echo '</urlset>'; ?>
@endif
