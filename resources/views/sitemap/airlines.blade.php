<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($airlines as $airline)
        <url>
            <loc>{{ url('/airlines/'.$airline->code) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
    @for($i = 1; $i <= ceil(count($airlines)/env('AIRLINES_LIMIT', 15)); $i++)
        <url>
            <loc>{{ url('/airlines/page/'.$i) }}</loc>
            <lastmod>{{ date('Y-m-d\TH:i:sP') }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.9</priority>
        </url>
    @endfor
</urlset>