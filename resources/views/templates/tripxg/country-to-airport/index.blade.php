@extends(config('template').'.layouts.app')

@section('content')
    @if(\Illuminate\Support\Facades\Request::segment(2))
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.countryToAirport.pageH1Title', ["name" => $from->name, "code" => $from->code, 'page' => $pagination['page'], 'siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.countryToAirport.pageH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'countryToAirport',
                    'url' => 'country-to-airport',
                    'props' => []
                ],
                [
                    'name' => 'countryToAirportPage',
                    'props' => [
                        'from' => $from->name,
                        'page' => $pagination['page']
                    ]
                ]
            ]
        ])
    @else
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.countryToAirport.listH1Title', ['siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.countryToAirport.listH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
           'items' => [
               [
                   'name' => 'countryToAirport',
                   'props' => []
               ]
           ]
       ])
    @endif

    @php
        $fromCities = [];
        foreach ($from->cities as $city) {
            $fromCities[] = "<a title='".trans('messages.cities.metaShowTitle',['name' => $city->name])."' href='".url('/cities/'.$city->code)."'>".$city->name."</a>";
        }

        $fromAirports = [];
        foreach ($from->airports as $airport) {
            $fromAirports[] = "<a title='".trans('messages.cities.metaShowTitle',['name' => $airport->name])."' href='".url('/cities/'.$airport->code)."'>".$airport->name."</a>";
        }
    @endphp

    <div class="content-container">
        <div class="app-fill">
            @if(\Illuminate\Support\Facades\Request::segment(2))
                <h3 class="primary-title">{{ trans('messages.countryToAirport.pageH3Title', ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]) }}</h3>
                <div class="page-content">
                    {!! trans('messages.countryToAirport.pageContent', [
                    "siteName" => env('SITE_NAME'),
                    "siteUrl" => url('/'),
                    "codes" => implode(', ',$flights->pluck('code')->toArray()),
                    "names" => implode(', ',$flights->pluck('name')->toArray()),
                    "name" => $from->name,
                    "code" => $from->code,
                    "page" => $pagination['page'],
                    "cities" => implode(', ', $fromCities),
                    "airports" => implode(', ', $fromAirports),
                    ]) !!}
                </div>
            @else
                <h3 class="primary-title">{{ trans('messages.countryToAirport.listH3Title') }}</h3>
                <div class="page-content">
                    {!! trans('messages.countryToAirport.listContent', [
                    "siteName" => env('SITE_NAME'),
                    "siteUrl" => url('/'),
                    "codes" => implode(', ',$flights->pluck('code')->toArray()),
                    "names" => implode(', ',$flights->pluck('name')->toArray()),
                    "name" => $from->name,
                    "code" => $from->code,
                    "cities" => implode(', ', $fromCities),
                    "airports" => implode(', ', $fromAirports),
                    ]) !!}
                </div>
            @endif
        </div>
    </div>

    <div class="flights-container">
        <div class="list-container flights-list">
            <div class="app-fill">
                @if(\Illuminate\Support\Facades\Request::segment(2))
                    <h4 class="secondary-title">{{ trans("messages.countryToAirport.pageH4Title", ["page" => $pagination["page"]]) }}</h4>
                @else
                    <h4 class="secondary-title">{{ trans("messages.countryToAirport.listH4Title") }}</h4>
                @endif
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row">
                        <div class="flights-table">
                            @foreach($flights as $flight)
                                <div class="flights-item">
                                    <div class="flights-link">
                                        <a title="{{ trans("messages.countryToAirport.metaShowTitle", ['from' => $from->name, 'to' => $flight->name]) }}"
                                           href="{{ url('/country-to-airport/'.$from->code.'/'.$flight->code) }}">
                                            <strong>{{ trans("messages.countryToAirport.metaShowTitle", ['from' => $from->name, 'to' => $flight->name]) }}</strong>
                                        </a>
                                    </div>

                                    <div class="flights-name">
                                        <strong>{{ $from->code. ' - ' .$flight->code}}</strong>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include (config('template').'/components/pagination/index', [
        'prevPageTitle' => trans("messages.countryToAirport.metaPageTitle", ["page" => $pagination["page"] - 1]),
        'currentPageTitle' => trans("messages.countryToAirport.pageH1Title", ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]),
        'nextPageTitle' => trans("messages.countryToAirport.metaPageTitle", ["page" => $pagination["page"] + 1]),
    ])
@endsection
