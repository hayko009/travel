@extends(config('template').'.layouts.app')

@section('content')

    @include(config('template').'/components/search/index', [
      'title' => trans('messages.flights.showH1Title', ['from' => $from->name, 'to' => $to->name]),
      'paragraph' => trans('messages.flights.showH2Title', ['from' => $from->name, 'fromCode' => $from->code, 'to' => $to->name, 'toCode' => $to->code]),
    ])

    @include(config('template').'/components/breadcrumbs/index', [
        'items' => [
            [
                'name' => 'country',
                'url' => 'countries/'.$from->country->code,
                'props' => [
                    'name' => $from->country->name
                ]
            ],
            [
                'name' => 'city',
                'url' => 'cities/'.$from->code,
                'props' => [
                    'name' => $from->name
                ]
            ],
            [
               'name' => 'flights',
               'url' => 'flights',
               'props' => []
            ],
            [
               'name' => 'flight',
               'props' => [
                   'from' => $from->name,
                   'to' => $to->name
               ]
            ]
        ]
    ])

    @include(config('template').'/components.calendar.index', ['from' => $from, 'to' => $to])
    @php
       $fromAirports = [];
       foreach ($from->airports as $airport) {
           $fromAirports[] = "<a title='".trans('messages.airports.metaShowTitle',['name' => $airport->name])."' href='".url('/airports/'.$airport->code)."'>".$airport->name."</a>";
       }

       $toAirports = [];
       foreach ($to->airports as $airport) {
           $toAirports[] = "<a title='".trans('messages.airports.metaShowTitle',['name' => $airport->name])."' href='".url('/airports/'.$airport->code)."'>".$airport->name."</a>";
       }
    @endphp

    <div class="content-container">
        <div class="app-fill">
            <h3 class="primary-title">{{ trans('messages.flights.showH3Title', ['from' => $from->name, 'fromCode' => $from->code, 'to' => $to->name, 'toCode' => $to->code]) }}</h3>
            <div class="page-content">
                {!! trans('messages.flights.showContent', [
                     "siteName" => env('SITE_NAME'),
                     "siteUrl" => url('/'),
                     "fromName" => $from->name,
                     "fromCountry" => $from->country->name,
                     "fromAirportsLinks" => implode(', ',$fromAirports),
                     "fromAirports" => implode(', ',$from->airports->pluck('name')->toArray()),
                     "fromTimeZone" => $from->params['timeZone'],
                     "fromCode" => $from->code,
                     "toName" => $to->name,
                     "toCountry" => $to->country->name,
                     "toAirportsLinks" => implode(', ',$toAirports),
                     "toAirports" => implode(', ',$to->airports->pluck('name')->toArray()),
                     "toTimeZone" => $to->params['timeZone'],
                     "toCode" => $to->code,
                ]) !!}
            </div>
        </div>
    </div>

    <div class="flights-container">
        <div class="list-container flights-list">
            <div class="app-fill">
                <h4 class="secondary-title">{{ trans('messages.randomCombinations.flights_from', ['name' => $from->name]) }}</h4>
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($fromCombinations as $flight)
                            <div class="flight-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.flights.metaShowTitle", ["from" => $flight['from']['name'], "to" => $flight['to']['name']]) }}"
                                   href="{{ url('/flights/'.$flight['from']['code'].'/'.$flight['to']['code']) }}">
                                    <div class="image-wrapper">
                                        <img class="from-img lazy-load"
                                             alt="{{ trans("messages.cities.metaShowTitle", ["name" => $flight['from']['name']]) }}"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ !empty($flight['from']['img'])? url('/images/cities/'.$flight['from']['img']) : url('/images/cities/default.jpg') }}">

                                        <img class="to-img lazy-load"
                                             alt="{{ trans("messages.cities.metaShowTitle", ["name" => $flight['to']['name']]) }}"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ !empty($flight['to']['img'])? url('/images/cities/'.$flight['to']['img']) : url('/images/cities/default.jpg') }}">
                                    </div>
                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.flights.metaShowTitle", ["from" => $flight['from']['name'], "to" => $flight['to']['name']]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="list-container flights-list">
            <div class="app-fill">
                <h4 class="secondary-title">{{ trans('messages.randomCombinations.flights_to', ['name' => $to->name]) }}</h4>
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($toCombinations as $flight)
                            <div class="flight-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.flights.metaShowTitle", ["from" => $flight['from']['name'], "to" => $flight['to']['name']]) }}"
                                   href="{{ url('/flights/'.$flight['from']['code'].'/'.$flight['to']['code']) }}">
                                    <div class="image-wrapper">
                                        <img class="from-img lazy-load"
                                             alt="{{ trans("messages.flights.metaShowTitle", ["name" => $flight['from']['name']]) }}"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ !empty($flight['from']['img'])? url('/images/cities/'.$flight['from']['img']) : url('/images/cities/default.jpg') }}">

                                        <img class="to-img lazy-load"
                                             alt="{{ trans("messages.flights.metaShowTitle", ["name" => $flight['to']['name']]) }}"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ !empty($flight['to']['img'])? url('/images/cities/'.$flight['to']['img']) : url('/images/cities/default.jpg') }}">
                                    </div>

                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.flights.metaShowTitle", ["from" => $flight['from']['name'], "to" => $flight['to']['name']]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
