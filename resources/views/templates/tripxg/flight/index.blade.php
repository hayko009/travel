@extends(config('template').'.layouts.app')

@section('content')
    @if(\Illuminate\Support\Facades\Request::segment(2))
        @include(config('template').'/components/search/index', [
         'title' => trans('messages.flights.pageH1Title', ["name" => $from->name, "code" => $from->code, 'page' => $pagination['page'], 'siteName' => env('SITE_NAME')]),
         'paragraph' => trans('messages.flights.pageH2Title')
       ])

        @include(config('template').'/components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'flights',
                    'url' => 'flights',
                    'props' => []
                ],
                [
                    'name' => 'flightsPage',
                    'props' => [
                        'from' => $from->name,
                        'page' => $pagination['page']
                    ]
                ]
            ]
        ])
    @else
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.flights.listH1Title', ['siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.flights.listH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
           'items' => [
               [
                   'name' => 'flights',
                   'props' => []
               ]
           ]
       ])
    @endif

    <div class="content-container">
        <div class="app-fill">
            @if(\Illuminate\Support\Facades\Request::segment(2))
                <h3 class="primary-title">{{ trans('messages.flights.pageH3Title', ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]) }}</h3>
                <div class="page-content">
                    {!! trans('messages.flights.pageContent', [
                    "siteName" => env('SITE_NAME'),
                    "siteUrl" => url('/'),
                    "codes" => implode(', ',$flights->pluck('code')->toArray()),
                    "names" => implode(', ',$flights->pluck('name')->toArray()),
                    "name" => $from->name,
                    "code" => $from->code,
                    ]) !!}
                </div>
            @else
                <h3 class="primary-title">{{ trans('messages.flights.listH3Title') }}</h3>
                <div class="page-content">
                    {!! trans('messages.flights.listContent', [
                    "siteName" => env('SITE_NAME'),
                    "siteUrl" => url('/'),
                    "codes" => implode(', ',$flights->pluck('code')->toArray()),
                    "names" => implode(', ',$flights->pluck('name')->toArray()),
                    "name" => $from->name,
                    "code" => $from->code,
                    ]) !!}
                </div>
            @endif
        </div>
    </div>

    <div class="flights-container">
        <div class="list-container flights-list">
            <div class="app-fill">
                @if(\Illuminate\Support\Facades\Request::segment(2))
                    <h4 class="secondary-title">{{ trans("messages.flights.pageH4Title", ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]) }}</h4>
                @else
                    <h4 class="secondary-title">{{ trans("messages.flights.listH4Title") }}</h4>
                @endif
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($flights as $flight)
                            <div class="flight-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.flights.metaShowTitle", ["from" => $from->name, "to" => $flight->name]) }}"
                                   href="{{ url('/flights/'.$from->code.'/'.$flight->code) }}">
                                    <div class="image-wrapper">
                                        <img class="from-img lazy-load"
                                             alt="{{ trans("messages.cities.metaShowTitle", ["name" => $from->name]) }}"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ !empty($from->img)? url('/images/cities/'.$from->img) : url('/images/cities/default.jpg') }}">

                                        <img class="to-img lazy-load"
                                             alt="{{ trans("messages.cities.metaShowTitle", ["name" => $flight->name]) }}"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ !empty($flight->img)? url('/images/cities/'.$flight->img) : url('/images/cities/default.jpg') }}">
                                    </div>
                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.flights.metaShowTitle", ["from" => $from->name, "to" => $flight->name]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include (config('template').'/components/pagination/index', [
        'prevPageTitle' => trans("messages.flights.metaPageTitle", ["page" => $pagination["page"] - 1]),
        'currentPageTitle' => trans("messages.flights.pageH1Title", ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]),
        'nextPageTitle' => trans("messages.flights.metaPageTitle", ["page" => $pagination["page"] + 1]),
    ])
@endsection
