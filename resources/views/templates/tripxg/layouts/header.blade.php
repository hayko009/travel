<div class="app-header">
    <div class="app-fill">
        <div class="app-header-wrapper">
            <div class="header-section">
                @if(url()->current() == url('/'))
                    <div class="app-logo">
                        <img class="app-logo-image"
                             src="{{ asset('/images/'.config('template').'/logo.png') }}"/>
                    </div>
                @else
                    <a href="{{url('/')}}"
                       class="app-logo">
                        <img class="app-logo-image"
                             src="{{ asset('/images/'.config('template').'/logo.png') }}"/>
                    </a>
                @endif
            </div>

            <div id="mobile-menu"
                 data-activates="slide-out"
                 class="header-section">
                <div class="header-menu-item">
                    <i class="fa fa-bars"></i>
                </div>
            </div>

            <div id="web-menu"
                 class="header-section">
                @foreach(trans('messages.layouts.menu') as $menuItem)
                    @if(url()->current() == url('/'.$menuItem["link"]))
                        <div class="header-menu-item active">
                            {{$menuItem["title"]}}
                        </div>
                    @else
                        <a href="{{ url('/'.$menuItem["link"])}}"
                           class="header-menu-item">
                            {{$menuItem["title"]}}
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>

<div id="slide-out"
     class="side-nav header-side-nav">

    @if(url()->current() == url('/'))
        <div class="mobile-logo">
            <img class="app-logo-image" src="{{ asset('/images/'.config('template').'/logo.png') }}"/>
        </div>
    @else
        <a href="{{url('/')}}"
           class="mobile-logo">
            <img class="app-logo-image" src="{{ asset('/images/'.config('template').'/logo.png') }}"/>
        </a>
    @endif

    <ul class="mobile-menu">
        @foreach(trans('messages.layouts.menu') as $menuItem)
            <li class="mobile-menu-item">
                @if(url()->current() == url('/'.$menuItem["link"]))
                    <div class="menu-link active">
                        {{ $menuItem["title"] }}
                    </div>
                @else
                    <a class="menu-link"
                       href="{{ url('/'.$menuItem["link"]) }}">
                        {{ $menuItem["title"] }}
                    </a>
                @endif
            </li>
        @endforeach
    </ul>
</div>
