@include(config('template').'.layouts.head')
<div class="search-results" id="search-results">
    <script type="text/javascript" src="{{ env('MIX_FLIGHTS_SEARCH_HOST') }}/iframe.js" async></script>
</div>
@include(config('template').'.layouts.foot')
