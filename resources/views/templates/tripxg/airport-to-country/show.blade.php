@extends(config('template').'.layouts.app')

@section('content')

    @include(config('template').'/components/search/index', [
     'title' => trans('messages.airportToCountry.showH1Title', ['from' => $from->name, 'to' => $to->name]),
     'paragraph' => trans('messages.airportToCountry.showH2Title', ['from' => $from->name, 'fromCode' => $from->code, 'to' => $to->name, 'toCode' => $to->code]),
    ])

    @include(config('template').'/components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'airportToCountry',
                    'url' => 'airport-to-country',
                    'props' => []
                ],
                [
                    'name' => 'airportToCountryShow',
                    'props' => [
                        'from' => $from->name,
                        'to' => $to->name
                    ]
                ]
            ]
        ])

    @php
        $fromAirports = [];
        foreach ($from->country->airports as $airport) {
            $fromAirports[] = "<a title='".trans('messages.airports.metaShowTitle',['name' => $airport->name])."' href='".url('/airports/'.$airport->code)."'>".$airport->name."</a>";
        }

        $toAirports = [];
        foreach ($to->airports as $airport) {
            $toAirports[] = "<a title='".trans('messages.airports.metaShowTitle',['name' => $airport->name])."' href='".url('/airports/'.$airport->code)."'>".$airport->name."</a>";
        }
    @endphp

    <div class="content-container">
        <div class="app-fill">
            <h3 class="primary-title">{{ trans('messages.airportToCountry.showH3Title', ['from' => $from->name, 'fromCode' => $from->code, 'to' => $to->name, 'toCode' => $to->code]) }}</h3>
            <div class="page-content">
                {!! trans('messages.airportToCountry.showContent', [
                     "siteName" => env('SITE_NAME'),
                     "siteUrl" => url('/'),
                     "fromName" => $from->name,
                     "fromCountry" => $from->country->name,
                     "fromCity" => $from->city->name,
                     "fromAirportsLinks" => implode(', ',$fromAirports),
                     "fromAirports" => implode(', ',$from->city->airports->pluck('name')->toArray()),
                     "fromTimeZone" => $from->params['timeZone'],
                     "fromCode" => $from->code,
                     "toName" => $to->name,
                     "toAirportsLinks" => implode(', ',$toAirports),
                     "toAirports" => implode(', ',$to->airports->pluck('name')->toArray()),
                     "toCode" => $to->code,
                ]) !!}
            </div>
        </div>
    </div>

    <div class="flights-container">
        <div class="list-container flights-list">
            <div class="app-fill">
                <h4 class="secondary-title">{{ trans('messages.randomCombinations.airportToCity', ['name' => $from->name]) }}</h4>
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row">
                        <div class="combination-table">
                            @foreach($fromCombinations as $flight)
                                <div class="combination-row">
                                    <div class="combination-link">
                                        <a title="{{ trans('messages.airportToCountry.metaShowTitle', ["from" => $flight['from']['code'], "to" => $flight['to']['code']]) }}"
                                           href="{{ url('/airport-to-city/'.$flight['from']['code'].'/'.$flight['to']['code']) }}">
                                            {{ trans('messages.airportToCountry.metaShowTitle', ["from" => $flight['from']['code'], "to" => $flight['to']['code']]) }}
                                        </a>
                                    </div>
                                    <div class="combination-name">
                                        <strong>{{ $flight['from']['name']. '-' .$flight['to']['name'] }}</strong>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="list-container flights-list">
            <div class="app-fill">
                <h2 class="secondary-title">{{ trans('messages.randomCombinations.cityToCountry', ['name' => $to->name]) }}</h2>
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row">
                        <div class="combination-table">
                            @foreach($toCombinations as $flight)
                                <div class="combination-row">
                                    <div class="combination-link">
                                        <a title="{{ trans('messages.cityToCountry.metaShowTitle', ["from" => $flight['from']['code'], "to" => $flight['to']['code']]) }}"
                                           href="{{ url('/city-to-country/'.$flight['from']['code'].'/'.$flight['to']['code']) }}">
                                            {{ trans('messages.cityToCountry.metaShowTitle', ["from" => $flight['from']['code'], "to" => $flight['to']['code']]) }}
                                        </a>
                                    </div>
                                    <div class="combination-name">
                                        <strong>{{ $flight['from']['name']. '-' .$flight['to']['name'] }}</strong>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
