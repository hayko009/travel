@extends(config('template').'.layouts.app')

@section('content')
    @if(\Illuminate\Support\Facades\Request::segment(2))
        @include(config('template').'/components/search/index', [
        'title' => trans('messages.airportToAirport.pageH1Title', ["name" => $from->name, "code" => $from->code, 'page' => $pagination['page'], 'siteName' => env('SITE_NAME')]),
        'paragraph' => trans('messages.airportToAirport.pageH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'airportToAirport',
                    'url' => 'airport-to-airport',
                    'props' => []
                ],
                [
                    'name' => 'airportToAirportPage',
                    'props' => [
                        'from' => $from->name,
                        'page' => $pagination['page']
                    ]
                ]
            ]
        ])
    @else
        @include(config('template').'/components/search/index', [
          'title' => trans('messages.airportToAirport.listH1Title', ['siteName' => env('SITE_NAME')]),
          'paragraph' => trans('messages.airportToAirport.listH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
           'items' => [
               [
                   'name' => 'airportToAirport',
                   'props' => []
               ]
           ]
       ])
    @endif

    @php
        $cityLink = '<a href="'.url('/cities/'.$from->city->code).'" title="'.trans('messages.cities.metaShowTitle', ['name' => $from->city->code]).'"><strong>'.$from->city->name.' ('.$from->city->code.') </strong></a>';
        $countryLink = '<a href="'.url('/countries/'.$from->country->code).'" title="'.trans('messages.countries.metaShowTitle', ['name' => $from->city->code]).'"><strong>'.$from->country->name.' ('.$from->country->code.') </strong></a>';
    @endphp

    <div class="content-container">
        <div class="app-fill">
            @if(\Illuminate\Support\Facades\Request::segment(2))
                <h3 class="primary-title">{{ trans('messages.airportToAirport.pageH3Title', ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]) }}</h3>
                <div class="page-content">
                    {!! trans('messages.airportToAirport.pageContent', [
                    "siteName" => env('SITE_NAME'),
                    "siteUrl" => url('/'),
                    "codes" => implode(', ',$flights->pluck('code')->toArray()),
                    "names" => implode(', ',$flights->pluck('name')->toArray()),
                    "name" => $from->name,
                    "code" => $from->code,
                    "page" => $pagination['page'],
                    "cityLink" => $cityLink,
                    "countryLink" => $countryLink,
                    ]) !!}
                </div>
            @else
                <h3 class="primary-title">{{ trans('messages.airportToAirport.listH3Title') }}</h3>
                <div class="page-content">
                    {!! trans('messages.airportToAirport.listContent', [
                    "siteName" => env('SITE_NAME'),
                    "siteUrl" => url('/'),
                    "codes" => implode(', ',$flights->pluck('code')->toArray()),
                    "names" => implode(', ',$flights->pluck('name')->toArray()),
                    "name" => $from->name,
                    "code" => $from->code,
                    "cityLink" => $cityLink,
                    "countryLink" => $countryLink,
                    ]) !!}
                </div>
            @endif
        </div>
    </div>

    <div class="flights-container">

        <div class="list-container flights-list">
            <div class="app-fill">
                @if(\Illuminate\Support\Facades\Request::segment(2))
                    <h4 class="secondary-title">{{ trans("messages.airportToAirport.pageH4Title", ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]) }}</h4>
                @else
                    <h4 class="secondary-title">{{ trans("messages.airportToAirport.listH4Title") }}</h4>
                @endif
            </div>


            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row">
                        <div class="flights-table">
                            @foreach($flights as $flight)
                                <div class="flights-item">
                                    <div class="flights-link">
                                        <a title="{{ trans("messages.airportToAirport.metaShowTitle", ['from' => $from->name, 'to' => $flight->name]) }}"
                                           href="{{ url('/airport-to-airport/'.$from->code.'/'.$flight->code) }}">
                                            <strong>{{ trans("messages.airportToAirport.metaShowTitle", ['from' => $from->name, 'to' => $flight->name]) }}</strong>
                                        </a>
                                    </div>

                                    <div class="flights-name">
                                        <strong>{{ $from->code. ' - ' .$flight->code }}</strong>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include (config('template').'/components/pagination/index', [
        'prevPageTitle' => trans("messages.airportToAirport.metaPageTitle", ["page" => $pagination["page"] - 1]),
        'currentPageTitle' => trans("messages.airportToAirport.pageH1Title", ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]),
        'nextPageTitle' => trans("messages.airportToAirport.metaPageTitle", ["page" => $pagination["page"] + 1]),
    ])
@endsection
