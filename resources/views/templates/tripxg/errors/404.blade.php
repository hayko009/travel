<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>{{ trans('not-found.scss') }}</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="yandex-verification" content="efaaedf276c9977b" />
    <meta name="viewport" content="width=device-width, target-densitydpi=device-dpi, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{ trans('messages.404.metaDescription') }}">
    <meta name="keywords" content="{{ trans('messages.404.metaKeywords') }}">

    <link rel="icon" href="{{ asset('images/favicon.png') }}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="not-found">


@include(config('template').'/components/not-found/index', [
     'title' => trans('messages.notFound.metaShowTitle'),
     'paragraph' => trans('messages.notFound.pageTitle')
])

<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(52318219, "init", {
        id:52318219,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>


<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" async></script>
</body>
</html>
