@extends(config('template').'.layouts.app')

@section('content')
    @if(\Illuminate\Support\Facades\Request::segment(2))
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.cityToCity.pageH1Title', ["name" => $from->name, "code" => $from->code, 'page' => $pagination['page'], 'siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.cityToCity.pageH2Title')
        ])


        @include(config('template').'/components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'cityToCity',
                    'url' => 'city-to-city',
                    'props' => []
                ],
                [
                    'name' => 'cityToCityPage',
                    'props' => [
                        'from' => $from->name,
                        'page' => $pagination['page']
                    ]
                ]
            ]
        ])
    @else
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.cityToCity.listH1Title', ['siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.cityToCity.listH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
           'items' => [
               [
                   'name' => 'cityToCity',
                   'props' => []
               ]
           ]
       ])
    @endif

    @php
        $airportsLinks = [];

        foreach ($from->airports as $airport) {
            $airportsLinks[] = "<a title='".trans('messages.airports.metaShowTitle',['name' => $airport->name])."' href='".url('/airports/'.$airport->code)."'>".$airport->name."</a>";
        }

        $countryLink = '<a href="'.url('/countries/'.$from->country->code).'" title="'.trans('messages.countries.metaShowTitle', ['name' => $from->code]).'"><strong>'.$from->country->name.' ('.$from->country->code.') </strong></a>';
    @endphp

    <div class="content-container">
        <div class="app-fill">
            @if(\Illuminate\Support\Facades\Request::segment(2))
                <h3 class="primary-title">{{ trans('messages.cityToCity.pageH3Title', ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]) }}</h3>
                <div class="page-content">
                    {!! trans('messages.cityToCity.pageContent', [
                    "siteName" => env('SITE_NAME'),
                    "siteUrl" => url('/'),
                    "codes" => implode(', ',$flights->pluck('code')->toArray()),
                    "names" => implode(', ',$flights->pluck('name')->toArray()),
                    "name" => $from->name,
                    "code" => $from->code,
                    "page" => $pagination['page'],
                    "airportsLinks" => implode(', ', $airportsLinks),
                    "countryLink" => $countryLink,
                    ]) !!}
                </div>
            @else
                <h3 class="primary-title">{{ trans('messages.cityToCity.listH3Title') }}</h3>
                <div class="page-content">
                    {!! trans('messages.cityToCity.listContent', [
                    "siteName" => env('SITE_NAME'),
                    "siteUrl" => url('/'),
                    "codes" => implode(', ',$flights->pluck('code')->toArray()),
                    "names" => implode(', ',$flights->pluck('name')->toArray()),
                    "name" => $from->name,
                    "code" => $from->code,
                    "airportsLinks" => implode(', ', $airportsLinks),
                    "countryLink" => $countryLink,
                    ]) !!}
                </div>
            @endif
        </div>
    </div>

    <div class="flights-container">
        <div class="list-container flights-list">
            <div class="app-fill">
                @if(\Illuminate\Support\Facades\Request::segment(2))
                    <h4 class="secondary-title">{{ trans("messages.cityToCity.pageH4Title", ["page" => $pagination["page"]]) }}</h4>
                @else
                    <h4 class="secondary-title">{{ trans("messages.cityToCity.listH4Title") }}</h4>
                @endif
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row">
                        <div class="flights-table">
                            @foreach($flights as $flight)
                                <div class="flights-item">
                                    <div class="flights-link">
                                        <a title="{{ trans("messages.cityToCity.metaShowTitle", ['from' => $from->name, 'to' => $flight->name]) }}"
                                           href="{{ url('/city-to-city/'.$from->code.'/'.$flight->code) }}">
                                            <strong>{{ trans("messages.cityToCity.metaShowTitle", ['from' => $from->name, 'to' => $flight->name]) }}</strong>
                                        </a>
                                    </div>

                                    <div class="flights-name">
                                        <strong>{{ $from->code. ' - ' .$flight->code}}</strong>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include (config('template')."/components/pagination/index", [
        'prevPageTitle' => trans("messages.cityToCity.metaPageTitle", ["page" => $pagination["page"] - 1]),
        'currentPageTitle' => trans("messages.cityToCity.pageH1Title", ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]),
        'nextPageTitle' => trans("messages.cityToCity.metaPageTitle", ["page" => $pagination["page"] + 1]),
    ])
@endsection
