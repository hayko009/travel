<div class="component-search" style="{{ !empty($img)? 'background-image:url("'.$img.'")': ''  }}">
    <div class="app-fill">
        <div class="search-container">
            <h1 class="primary-title light-text">
                {!! $title !!}
            </h1>
        </div>
    </div>

    @include(config('template')."/components/flights/index")
</div>
