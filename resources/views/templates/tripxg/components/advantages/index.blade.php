<div class="component-advantages">
    <div class="app-fill">
        <div class="advantages-wrapper">
            <div class="advantages-items">
                <div class="app-row">
                    @foreach(trans('messages.layouts.advantages') as $advantage)
                        <div class="advantage-item app-column large-4 middle-6 small-12">
                            <div class="advantage-box">
                                <div class="advantage-count">
                                    <button>
                                        {!! $advantage["count"] !!}
                                    </button>
                                </div>
                                <div class="advantage-title">
                                    {!! $advantage["title"] !!}
                                </div>
                                <div class="advantage-text">
                                    {!! $advantage["text"] !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
