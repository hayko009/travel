<div class="component-hits">
    <div class="app-fill">
        <div class="hits-wrapper">
            <h2 class="secondary-title dark-text">
                {{ trans('messages.layouts.hitsTitle') }}
            </h2>
            <div class="hit-items">
                <div class="app-row">
                    @foreach(trans('messages.hitFlights') as $cityCode)
                        <div class="hit-item app-column large-3 middle-6 small-12">
                            <script async src="//{!! env('TRAVELPAYOUTS_HOST_URL') !!}/weedle/widget.js?marker={!! env('APP_SEARCH_MARKER') !!}&host={!! env('SUBDOMAIN_SEARCH_HOST') !!}&locale={!! config('locale') !!}&currency={!! config('currency') !!}&powered_by=false&destination={!! $cityCode !!}" charset="UTF-8"></script>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
