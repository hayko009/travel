<div class="app-copyright">
    {{ trans('messages.layouts.copyRight', ['date' => date('Y'),'site' => env('SITE_NAME')]) }}
</div>
