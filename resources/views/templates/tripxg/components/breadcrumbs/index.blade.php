<div class="breadcrumbs">
    <div class="app-fill">
        <ol itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement"
                itemscope
                itemtype="http://schema.org/ListItem">
                <a itemprop="item"
                   title="{{ trans('messages.breadcrumbs.home') }}"
                   href="{{ url('/') }}">
                    <span itemprop="name">{{ trans('messages.breadcrumbs.home') }}</span>
                </a>
                <i class="fa fa-angle-right"></i>
            </li>

            @foreach($items as $key => $item)
                <li itemprop="itemListElement"
                    itemscope
                    itemtype="http://schema.org/ListItem">
                    @if($key == count($items) - 1)
                        <span itemprop="name">{{ trans('messages.breadcrumbs.'.$item['name'], $item['props']) }}</span>
                    @else
                        <a itemprop="item"
                           title="{{ trans('messages.breadcrumbs.'.$item['name'], $item['props']) }}"
                           href="{{ url('/'.$item['url']) }}">
                            <span itemprop="name">{{ trans('messages.breadcrumbs.'.$item['name'], $item['props']) }}</span>
                        </a>
                        <i class="fa fa-angle-right"></i>
                    @endif
                </li>
            @endforeach
        </ol>
    </div>
</div>
