@extends(config('template').'.layouts.app')

@section('content')

    @include(config('template').'/components/search/index', [
         'title' => trans('messages.airlines.showH1Title', ['name' => $airline->name]),
         'paragraph' => trans('messages.airlines.showH2Title', ['name' => $airline->name])
    ])

    @include(config('template').'/components/breadcrumbs/index', [
        'items' => [
            [
                'name' => 'airlines',
                'url' => 'airlines',
                'props' => []
            ],
            [
                'name' => 'airline',
                'props' => [
                    'name' => $airline->name
                ]
            ]
        ]
    ])

    <div class="content-container">
        <div class="app-fill">
            <h3 class="primary-title">{{ trans('messages.airports.showH3Title', ['name' => $airline->name]) }}</h3>
            <div class="page-content">
                {!! trans('messages.airlines.showContent', [
                     "siteName" => env('SITE_NAME'),
                     "siteUrl" => url('/'),
                     "name" => $airline->name,
                     "code" => $airline->code,
                     "alt" => trans('messages.airlines.metaShowTitle', ['name' => $airline->name]),
                     "img" => url('/images/airlines/'.$airline->img),
                ]) !!}
            </div>
        </div>
    </div>

    <div class="airlines-container">
        <div class="list-container airlines-list">
            <div class="app-fill">
                <h4 class="primary-title">{{ trans('messages.airlines.showH4Title') }}</h4>
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($randomAirlines as $airline)
                            <div class="airline-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.airlines.metaShowTitle", ["name" => $airline->name]) }}"
                                   href="{{ url('/airlines/'.$airline->code) }}">
                                    <div class="image-wrapper">
                                        <img alt="{{ trans("messages.airlines.metaShowTitle", ["name" => $airline->name]) }}"
                                             class="lazy-load"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ url('/images/airlines/'.$airline->img) }}">
                                    </div>
                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.airlines.metaShowTitle", ["name" => $airline->name]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
