@extends(config('template').'.layouts.app')

@section('content')

    @if(\Illuminate\Support\Facades\Request::segment(2))
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.airports.pageH1Title', ['page' => $pagination['page'], 'siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.airports.pageH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'airports',
                    'url' => 'airports',
                    'props' => []
                ],
                [
                    'name' => 'page',
                    'props' => [
                        'page' => $pagination['page']
                    ]
                ]
            ]
        ])
    @else
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.airports.listH1Title', ['siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.airports.listH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
           'items' => [
               [
                   'name' => 'airports',
                   'props' => []
               ]
           ]
       ])
    @endif

    <div class="content-container">
        <div class="app-fill">
            <h4 class="primary-title">{{ trans('messages.airports.listH3Title') }}</h4>
            <div class="page-content">
                {!! trans('messages.airports.listContent', [
                     "siteName" => env('SITE_NAME'),
                     "siteUrl" => url('/'),
                     "codes" => implode(', ',$airports->pluck('code')->toArray()),
                     "names" => implode(', ',$airports->pluck('name')->toArray()),
                ]) !!}
            </div>
        </div>
    </div>

    <div class="airports-container">
        <div class="list-container airports-list">
            <div class="app-fill">
                @if(\Illuminate\Support\Facades\Request::segment(2))
                    <h4 class="secondary-title">{{ trans("messages.airports.pageH4Title", ["page" => $pagination["page"]]) }}</h4>
                @else
                    <h4 class="secondary-title">{{ trans("messages.airports.listH4Title") }}</h4>
                @endif
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($airports as $airport)
                            <div class="airport-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.airports.metaShowTitle", ["name" => $airport->name]) }}"
                                   href="{{ url('/airports/'.$airport->code) }}">
                                    <div class="image-wrapper">
                                        <img alt="{{ trans("messages.airports.metaShowTitle", ["name" => $airport->name]) }}"
                                             class="lazy-load"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ url('/images/airports/airport.jpg') }}">
                                    </div>
                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.airports.metaShowTitle", ["name" => $airport->name]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include (config('template').'/components/pagination/index', [
        'prevPageTitle' => trans("messages.airports.metaPageTitle", ['siteName' => env('SITE_NAME'), "page" => $pagination["page"] - 1]),
        'currentPageTitle' => trans("messages.airports.pageH1Title", ["page" => $pagination["page"]]),
        'nextPageTitle' => trans("messages.airports.metaPageTitle", ['siteName' => env('SITE_NAME'), "page" => $pagination["page"] + 1]),
    ])
@endsection
