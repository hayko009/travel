@extends(config('template').'.layouts.app')

@section('content')

    @include(config('template').'/components/search/index', [
        'title' => trans('messages.airports.showH1Title', ['name' => $airport->name]),
        'paragraph' => trans('messages.airports.showH2Title', ['name' => $airport->name])
   ])

    @include(config('template').'/components/breadcrumbs/index', [
        'items' => [
            [
                'name' => 'airports',
                'url' => 'airports',
                'props' => []
            ],
            [
                'name' => 'airport',
                'props' => [
                    'name' => $airport->name
                ]
            ]
        ]
    ])

    <div class="content-container">
        <div class="app-fill">
            <h3 class="primary-title">{{ trans('messages.airports.showH3Title', ['name' => $airport->name]) }}</h3>
            <div class="page-content">
                {!! trans('messages.airports.showContent', [
                     "siteName" => env('SITE_NAME'),
                     "siteUrl" => url('/'),
                     "name" => $airport->name,
                     "mapLink" => 'https://www.google.com/maps/embed/v1/place?key='.env('GOOGLE_MAP_KEY').'&q='.str_replace(" ", "+", $airport->name).",".str_replace(" ", "+", $airport->country->name."+".$airport->city->name).'&zoom=14&maptype=roadmap',
                     "country" => $airport->country->name,
                     "city" => $airport->city->name,
                     "lat" => $airport->params['coordinates']['lat']??'',
                     "lon" => $airport->params['coordinates']['lon']??'',
                     "timeZone" => $airport->params['timeZone'],
                     "code" => $airport->code,
                ]) !!}
            </div>
        </div>
    </div>

    <div class="airports-container">
        <div class="list-container airports-list">
            <div class="app-fill">
                <h4 class="primary-title">{{ trans('messages.airports.showH4Title') }}</h4>
            </div>


            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($randomAirports as $airport)
                            <div class="airport-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.airports.metaShowTitle", ["name" => $airport->name]) }}"
                                   href="{{ url('/airports/'.$airport->code) }}">
                                    <div class="image-wrapper">
                                        <img alt="{{ trans("messages.airports.metaShowTitle", ["name" => $airport->name]) }}"
                                             class="lazy-load"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ url('/images/airports/airport.jpg') }}">
                                    </div>
                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.airports.metaShowTitle", ["name" => $airport->name]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
