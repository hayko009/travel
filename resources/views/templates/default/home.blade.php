@extends(config('template').'.layouts.app')
@section('content')
    @include(config('template').'/components/search/index', [
    'title' => trans('messages.h1Title'),
    'paragraph' => trans('messages.h2Title')
    ])

    @include(config('template').'/components/calendar/index')
    @include(config('template').'/components/advantages/index')
    @include(config('template').'/components/popular/index')
    @include(config('template').'/components/subscribe/index')
    @include(config('template').'/components/social-icons/index')
@endsection
