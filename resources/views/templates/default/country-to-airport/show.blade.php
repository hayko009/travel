@extends(config('template').'.layouts.app')

@section('content')

    @include(config('template').'/components/search/index', [
       'title' => trans('messages.countryToAirport.showH1Title', ['from' => $from->name, 'to' => $to->name]),
       'paragraph' => trans('messages.countryToAirport.showH2Title', ['from' => $from->name, 'fromCode' => $from->code, 'to' => $to->name, 'toCode' => $to->code]),
    ])

    @include(config('template').'/components/breadcrumbs/index', [
               'items' => [
                   [
                       'name' => 'countryToAirport',
                       'url' => 'country-to-airport',
                       'props' => []
                   ],
                   [
                       'name' => 'countryToAirportShow',
                       'props' => [
                           'from' => $from->name,
                           'to' => $to->name
                       ]
                   ]
               ]
    ])


    @php
        $fromCities = [];
        foreach ($from->cities as $city) {
            $fromCities[] = "<a title='".trans('messages.airports.metaShowTitle',['name' => $city->name])."' href='".url('/airports/'.$city->code)."'>".$city->name."</a>";
        }

        $fromAirports = [];
        foreach ($from->airports as $airport) {
            $fromAirports[] = "<a title='".trans('messages.airports.metaShowTitle',['name' => $airport->name])."' href='".url('/airports/'.$airport->code)."'>".$airport->name."</a>";
        }

        $toAirports = [];
        foreach ($to->city->airports as $airport) {
            $toAirports[] = "<a title='".trans('messages.airports.metaShowTitle',['name' => $airport->name])."' href='".url('/airports/'.$airport->code)."'>".$airport->name."</a>";
        }
    @endphp

    <div class="content-container">
        <div class="app-fill">
            <h3 class="primary-title">{{ trans('messages.countryToAirport.showH3Title', ['from' => $from->name, 'fromCode' => $from->code, 'to' => $to->name, 'toCode' => $to->code]) }}</h3>
            <div class="page-content">
                {!! trans('messages.countryToAirport.showContent', [
                     "siteName" => env('SITE_NAME'),
                     "siteUrl" => url('/'),
                     "fromName" => $from->name,
                     "fromCities" => implode(', ', $fromCities),
                     "fromAirportsLinks" => implode(', ', $fromAirports),
                     "fromAirports" => implode(', ', $from->airports->pluck('name')->toArray()),
                     "fromCode" => $from->code,
                     "toName" => $to->name,
                     "toCity" => $to->city->name,
                     "toAirportsLinks" => implode(', ', $toAirports),
                     "toAirports" => implode(', ', $to->city->airports->pluck('name')->toArray()),
                     "toTimeZone" => $to->params['timeZone'],
                     "toCode" => $to->code,
                ]) !!}
            </div>
        </div>
    </div>

    <div class="flights-container">
        <div class="list-container flights-list">
            <div class="app-fill">
                <h4 class="secondary-title">{{ trans('messages.randomCombinations.countryToCity', ['name' => $from->name]) }}</h4>
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row">
                        <div class="combination-table">
                            @foreach($fromCombinations as $flight)
                                <div class="combination-row">
                                    <div class="combination-link">
                                        <a title="{{ trans('messages.countryToCity.metaShowTitle', ["from" => $flight['from']['code'], "to" => $flight['to']['code']]) }}"
                                           href="{{ url('/country-to-city/'.$flight['from']['code'].'/'.$flight['to']['code']) }}">
                                            {{ trans('messages.countryToCity.metaShowTitle', ["from" => $flight['from']['code'], "to" => $flight['to']['code']]) }}
                                        </a>
                                    </div>
                                    <div class="combination-name">
                                        <strong>{{ $flight['from']['name']. '-' .$flight['to']['name'] }}</strong>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="list-container flights-list">
            <div class="app-fill">
                <h2 class="secondary-title">{{ trans('messages.randomCombinations.cityToAirport', ['name' => $to->name]) }}</h2>
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row">
                        <div class="combination-table">
                            @foreach($toCombinations as $flight)
                                <div class="combination-row">
                                    <div class="combination-link">
                                        <a title="{{ trans('messages.cityToAirport.metaShowTitle', ["from" => $flight['from']['code'], "to" => $flight['to']['code']]) }}"
                                           href="{{ url('/city-to-airport/'.$flight['from']['code'].'/'.$flight['to']['code']) }}">
                                            {{ trans('messages.cityToAirport.metaShowTitle', ["from" => $flight['from']['code'], "to" => $flight['to']['code']]) }}
                                        </a>
                                    </div>

                                    <div class="combination-name">
                                        <strong>{{ $flight['from']['name']. '-' .$flight['to']['name'] }}</strong>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
