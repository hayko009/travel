@extends(config('template').'.layouts.app')

@section('content')
    @if(\Illuminate\Support\Facades\Request::segment(2))
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.countryToCity.pageH1Title', ["name" => $from->name, "code" => $from->code, 'page' => $pagination['page'], 'siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.countryToCity.pageH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'countryToCity',
                    'url' => 'country-to-city',
                    'props' => []
                ],
                [
                    'name' => 'countryToCityPage',
                    'props' => [
                        'from' => $from->name,
                        'page' => $pagination['page']
                    ]
                ]
            ]
        ])
    @else
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.countryToCity.listH1Title', ['siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.countryToCity.listH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
           'items' => [
               [
                   'name' => 'countryToCity',
                   'props' => []
               ]
           ]
       ])
    @endif

    @php
        $fromCities = [];
        foreach ($from->cities as $city) {
            $fromCities[] = "<a title='".trans('messages.cities.metaShowTitle',['name' => $city->name])."' href='".url('/cities/'.$city->code)."'>".$city->name."</a>";
        }
    @endphp

    <div class="content-container">
        <div class="app-fill">
            @if(\Illuminate\Support\Facades\Request::segment(2))
                <h3 class="primary-title">{{ trans('messages.countryToCity.pageH3Title', ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]) }}</h3>
                <div class="page-content">
                    {!! trans('messages.countryToCity.pageContent', [
                    "siteName" => env('SITE_NAME'),
                    "siteUrl" => url('/'),
                    "codes" => implode(', ',$flights->pluck('code')->toArray()),
                    "names" => implode(', ',$flights->pluck('name')->toArray()),
                    "name" => $from->name,
                    "code" => $from->code,
                    "page" => $pagination['page'],
                    "cities" => implode(', ', $fromCities),
                    ]) !!}
                </div>
            @else
                <h3 class="primary-title">{{ trans('messages.countryToCity.listH3Title') }}</h3>
                <div class="page-content">
                    {!! trans('messages.countryToCity.listContent', [
                    "siteName" => env('SITE_NAME'),
                    "siteUrl" => url('/'),
                    "codes" => implode(', ',$flights->pluck('code')->toArray()),
                    "names" => implode(', ',$flights->pluck('name')->toArray()),
                    "name" => $from->name,
                    "code" => $from->code,
                    "cities" => implode(', ', $fromCities),
                    ]) !!}
                </div>
            @endif
        </div>
    </div>

    <div class="flights-container">
        <div class="list-container flights-list">
            <div class="app-fill">
                @if(\Illuminate\Support\Facades\Request::segment(2))
                    <h4 class="secondary-title">{{ trans("messages.countryToCity.pageH4Title", ["page" => $pagination["page"]]) }}</h4>
                @else
                    <h4 class="secondary-title">{{ trans("messages.countryToCity.listH4Title") }}</h4>
                @endif
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row">
                        <div class="flights-table">
                            @foreach($flights as $flight)
                                <div class="flights-item">
                                    <div class="flights-link">
                                        <a title="{{ trans("messages.countryToCity.metaShowTitle", ['from' => $from->name, 'to' => $flight->name]) }}"
                                           href="{{ url('/country-to-city/'.$from->code.'/'.$flight->code) }}">
                                            <strong>{{ trans("messages.countryToCity.metaShowTitle", ['from' => $from->name, 'to' => $flight->name]) }}</strong>
                                        </a>
                                    </div>

                                    <div class="flights-name">
                                        <strong>{{ $from->code. ' - ' .$flight->code}}</strong>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include (config('template').'/components/pagination/index', [
        'prevPageTitle' => trans("messages.countryToCity.metaPageTitle", ["page" => $pagination["page"] - 1]),
        'currentPageTitle' => trans("messages.countryToCity.pageH1Title", ["name" => $from->name, "code" => $from->code, "page" => $pagination["page"]]),
        'nextPageTitle' => trans("messages.countryToCity.metaPageTitle", ["page" => $pagination["page"] + 1]),
    ])
@endsection
