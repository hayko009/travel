<div class="flights-form search-widget">
    <script charset="utf-8"
            type="text/javascript">
        window.TP_FORM_SETTINGS = window.TP_FORM_SETTINGS || {};
        window.TP_FORM_SETTINGS["{!! env('APP_SEARCH_HANDLE') !!}"] = {
            "handle": "{!! env('APP_SEARCH_HANDLE') !!}",
            "widget_name": "Поисковая форма #1",
            "border_radius": "0",
            "additional_marker": null,
            "width": null,
            "show_logo": false,
            "show_hotels": false,
            "form_type": "avia",
            "locale": "ru",
            "currency": "rub",
            "sizes": "default",
            "search_target": "_self",
            "active_tab": "avia",
            "search_in_iframe": true,
            "search_host": "{!! env('APP_URL') !!}/search",
            "hotels_host": "{!! env('APP_URL') !!}/search",
            "hotel": "",
            "hotel_alt": "Hotellook сравнивает цены на гостиницы среди более чем 30 агентств и находит лучший вариант",
            "avia_alt": "OneTwoClick - карта низких цен, удобный поиск дешевых билетов на самолет, расписание самолетов",
            "retargeting": true,
            "trip_class": "economy",
            "depart_date": null,
            "return_date": null,
            "check_in_date": null,
            "check_out_date": null,
            "no_track": false,
            "powered_by": false,
            "id": "{!! env('APP_SEARCH_ID') !!}",
            "marker": "{!! env('APP_SEARCH_MARKER') !!}",
            "origin": {
                "name": ""
            },
            "destination": {
                "name": "Париж",
                "iata": "PAR"
            },
            "color_scheme": {
                "name": "custom",
                "icons": "icons_white",
                "background": "{!! env("APP_SEARCH_BACKGROUND") !!}",
                "color": "{!! env("APP_SEARCH_COLOR") !!}",
                "border_color": "{!! env("APP_SEARCH_BORDER_COLOR") !!}",
                "button": "{!! env("APP_SEARCH_BUTTON") !!}",
                "button_text_color": "{!! env("APP_SEARCH_BUTTON_TEXT_COLOR") !!}",
                "input_border": "{!! env("APP_SEARCH_INPUT_BORDER") !!}"
            },
            "hotels_type": "hotellook_host",
            "best_offer": {
                "locale": "ru",
                "currency": "rub",
                "marker": "{!! env('APP_SEARCH_MARKER') !!}",
                "search_host": "{!! env('MIX_FLIGHTS_SEARCH_HOST') !!}/flights",
                "offers_switch": false,
                "api_url": "//www.travelpayouts.com/minimal_prices/offers.json",
                "routes": []
            },
            "hotel_logo_host": null,
            "search_logo_host": "www.aviasales.ru",
            "hotel_marker_format": null,
            "hotelscombined_marker": null,
            "responsive": true,
            "height": 600
        }
    </script>

    <script charset="utf-8"
            src="//www.travelpayouts.com/widgets/8d909cd91be701c2c774c1c6ceead0c2.js?v=1703" async>
    </script>
</div>
