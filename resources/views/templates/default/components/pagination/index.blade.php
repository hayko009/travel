<div class="pagination-component">
    <ul class="pagination">
        <li class="pagination-page prev-page">
            @if($pagination['prev']['url'])
                <a class="page-link"
                   href="{{ $pagination['prev']['url'] }}"
                   title="{{ $prevPageTitle }}">
                    <i class="fa fa-angle-left"></i>
                </a>
            @else
                <div class="page-name">
                    <i class="fa fa-angle-left"></i>
                </div>
            @endif
        </li>

        <li class="pagination-page page-title">
            @if(isset($currentPageTitle))
                {{ $currentPageTitle }}
            @else
                {{ trans('messages.pagination.page', ['page' => $pagination['page']]) }}
            @endif
        </li>

        <li class="pagination-page next-page">
            @if($pagination['next']['url'])
                <a class="page-link"
                   href="{{$pagination['next']['url']}}"
                   title="{{ $nextPageTitle }}">
                    <i class="fa fa-angle-right"></i>
                </a>
            @else
                <div class="page-name">
                    <i class="fa fa-angle-right"></i>
                </div>
            @endif
        </li>
    </ul>
</div>
