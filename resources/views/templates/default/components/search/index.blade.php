<div class="component-search" style="{{ !empty($img)? 'background-image:url("'.$img.'")': ''  }}">
    <div class="app-fill">
        <div class="search-container">
            <h1 class="primary-title light-text">
                {!! $title !!}
            </h1>

            <h2 class="secondary-title light-text">
                {!! $paragraph !!}
            </h2>

            <div id="flights-form">
                @include(config('template').".components.flights.index")
            </div>

            <div class="flights-stats">
                <div class="app-row">
                    @foreach(trans('messages.layouts.statistics') as $item)
                        <div class="app-column large-4 middle-4 small-12">
                            <div class="stat-item">
                                <div class="stat-icon">
                                    <i class="{{ $item['icon'] }}"></i>
                                </div>
                                <div class="stat-title">
                                    {!! $item['title'] !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
