<div class="component-not-found">
    <div class="app-fill">
        <div class="search-container">
            <h1 class="primary-title light-text">
                {!! $title !!}
            </h1>

            <p class="secondary-title light-text">
                {!! $paragraph !!}
            </p>

            <div id="flights-form-header">
                @include(config('template').".components.flights.index")
            </div>

            <div class="not-found-content">
                <div class="not-found-icon">
                    {{ trans('messages.randomWords.notFound') }}
                </div>
                <div class="home-button">
                    <a href="{{ url('/') }}"
                       title="{{ trans('messages.metaTitle') }}"
                       class="btn waves-effect">
                        {{ trans('messages.randomWords.homePage') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
