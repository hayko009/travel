<div class="component-advantages">
    <div class="app-fill">
        <div class="advantages-wrapper">
            <div class="advantages-content">
                <h2 class="primary-title light-text">
                    {{ trans('messages.layouts.advantageTitle') }}
                </h2>
                <ol class="advantages-list">
                    @foreach(trans('messages.layouts.advantages') as $advantage)
                        <li>
                            {{ $advantage }}
                        </li>
                    @endforeach
                </ol>
            </div>
            <div class="advantages-image"></div>
        </div>
    </div>
</div>
