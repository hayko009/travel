<div class="component-calendar">
    <div class="app-fill">
        <div class="calendar-wrapper">
            @if(isset($from) && isset($to))
                <h2 class="primary-title dark-text">
                    {{ trans('messages.randomWords.calendarEntityTitle', ['fromName' => $from->name, 'toName' => $to->name]) }}
                </h2>
            @else
                <h2 class="primary-title dark-text">
                    {{ trans('messages.randomWords.calendarGlobalTitle') }}
                </h2>
            @endif

            @if(empty($from) || empty($to))
                <div class="calendar-blocks">
                    <div class="app-row">
                        @foreach(trans('messages.layouts.calendar') as $item)
                            <div class="app-column large-3 middle-6 small-12">
                                <div class="calendar-item">
                                    <div class="calendar-circle">
                                        <div class="circle-number">{{ $item['number'] }}</div>
                                        <div class="circle-icon">
                                            <i class="{{ $item['icon'] }}"></i>
                                        </div>
                                    </div>
                                    <div class="calendar-text">{{ $item['title'] }}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif

            <div class="calendar-widget">
                @if(isset($from) && isset($to))
                    <script charset="utf-8" src="//www.travelpayouts.com/calendar_widget/iframe.js?marker=154446.&origin={{$from->code}}&destination={{$to->code}}&currency=rub&searchUrl={{ env('MIX_FLIGHTS_SEARCH_HOST') }}&one_way=false&only_direct=false&locale=ru&period=year&range=7%2C14&powered_by=false" async></script>
                @else
                    <script charset="utf-8" src="//www.travelpayouts.com/calendar_widget/iframe.js?marker=154446.&origin=MOW&destination=PAR&currency=rub&searchUrl={{ env('MIX_FLIGHTS_SEARCH_HOST') }}&one_way=false&only_direct=false&locale=ru&period=year&range=7%2C14&powered_by=false" async></script>
                @endif
            </div>
        </div>
    </div>
</div>
