<div class="component-social-icons">
    <div class="app-fill">
        <div class="social-icons-wrapper">
            <h2 class="secondary-title">{{ trans('messages.layouts.socialIconsTitle') }}</h2>
            <div class="social-icons-list">
                <a href="https://www.facebook.com/groups/164416120879694/"
                   class="social-link-item">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="https://vk.com/mix.trip"
                   class="social-link-item">
                    <i class="fab fa-vk"></i>
                </a>
                <a href="https://www.instagram.com/mix_trip_/"
                   class="social-link-item">
                    <i class="fab fa-instagram"></i>
                </a>
            </div>
        </div>
    </div>
</div>
