<div class="component-popular">
    <div class="app-fill">
        <div class="popular-wrapper">
            <h2 class="primary-title dark-text">
                Самые популярные направления перелётов
            </h2>
            <div class="popular-items">
                <div class="app-row slider-items">
                    <div class="popular-item app-column large-4 middle-6 small-12">
                        <script async src="//www.travelpayouts.com/weedle/widget.js?marker=66729&host={!! env('SITE_NAME') !!}&locale=ru&currency=rub&powered_by=false&destination=MRS&destination_name=%D0%9C%D0%B0%D1%80%D1%81%D0%B5%D0%BB%D1%8C" charset="UTF-8"></script>
                    </div>
                    <div class="popular-item app-column large-4 middle-6 small-12">
                        <script async src="//www.travelpayouts.com/weedle/widget.js?marker=66729&host={!! env('SITE_NAME') !!}&locale=ru&currency=rub&powered_by=false&destination=BKK&destination_name=%D0%91%D0%B0%D0%BD%D0%B3%D0%BA%D0%BE%D0%BA" charset="UTF-8"></script>
                    </div>
                    <div class="popular-item app-column large-4 middle-6 small-12">
                        <script async src="//www.travelpayouts.com/weedle/widget.js?marker=66729&host={!! env('SITE_NAME') !!}&locale=ru&currency=rub&powered_by=false&destination=HKT&destination_name=%D0%9F%D1%85%D1%83%D0%BA%D0%B5%D1%82" charset="UTF-8"></script>
                    </div>
                    <div class="popular-item app-column large-4 middle-6 small-12">
                        <script async src="//www.travelpayouts.com/weedle/widget.js?marker=66729&host={!! env('SITE_NAME') !!}&locale=ru&currency=rub&powered_by=false&destination=BCN&destination_name=%D0%91%D0%B0%D1%80%D1%81%D0%B5%D0%BB%D0%BE%D0%BD%D0%B0" charset="UTF-8"></script>
                    </div>
                    <div class="popular-item app-column large-4 middle-6 small-12">
                        <script async src="//www.travelpayouts.com/weedle/widget.js?marker=66729&host={!! env('SITE_NAME') !!}&locale=ru&currency=rub&powered_by=false&destination=BSB&destination_name=%D0%91%D1%80%D0%B0%D0%B7%D0%B8%D0%BB%D0%B8%D0%B0" charset="UTF-8"></script>
                    </div>
                    <div class="popular-item app-column large-4 middle-6 small-12">
                        <script async src="//www.travelpayouts.com/weedle/widget.js?marker=66729&host={!! env('SITE_NAME') !!}&locale=ru&currency=rub&powered_by=false&destination=IBZ&destination_name=%D0%98%D0%B1%D0%B8%D1%86%D0%B0" charset="UTF-8"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>