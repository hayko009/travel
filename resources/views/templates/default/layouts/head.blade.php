<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>{{ $meta['title'] }}</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="yandex-verification" content="efaaedf276c9977b" />
    <meta name="viewport" content="width=device-width, target-densitydpi=device-dpi, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{ $meta['description'] }}">
    <meta name="keywords" content="{{ $meta['keywords'] }}">

    <meta property="og:title" content="{{ $meta['title'] }}" />
    <meta property="og:site_name" content="{{ env('SITE_NAME') }}" />
    <meta property="og:description" content="{{ $meta['description'] }}" />
    <meta property="og:locale" content="{{ app()->getLocale() }}" />

    <link rel="icon" href="{{ asset('images/favicon.png') }}">

    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/loader.css') }}"/>

    <script rel="script" type="text/javascript" src="{{ asset('js/loader.js') }}"></script>

    <script rel="script" type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-134246826-1" async></script>
    <script rel="script" type="text/javascript">
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-134246826-1');
    </script>
    <!-- Google Tag Manager -->
    <script rel="script" type="text/javascript">
        (function(w,d,s,l,i){
            w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});
            var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),
                dl=l!='dataLayer'?'&l='+l:'';
            j.async=true;
            j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;
            f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W3W5M4G');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>
