<div class="app-footer">
    <div class="footer-links">
        <div class="app-fill">
            <div class="footer-links-wrapper">
                <div class="app-row">
                    @foreach($randomLinks as $data)
                        <div class="links-column app-column large-3 middle-6 small-12">
                            <div class="links-box">
                                <div class="links-box-wrapper">

                                    <p class="link-box-header">{{ trans('messages.randomLinks.'.$data['listName'].'.name') }}</p>

                                    @foreach($data['items'] as $item)
                                        <a class="link-item"
                                           title="{{ trans('messages.'.$data['listName'].'.metaShowTitle', ['siteName' => env('SITE_NAME'), 'name' => $item->name]) }}"
                                           href="{{ url('/'.$data['listName'].'/'.$item->code) }}">
                                            {{ $item->name }}
                                        </a>
                                    @endforeach
                                    @if(url()->current() == url('/'.$data['listName']))
                                        <div class="main-link active">
                                            <span class="name">{{ trans('messages.randomLinks.'.$data['listName'].'.all') }}</span>
                                            <i class="fa fa-paper-plane"></i>
                                        </div>
                                    @else
                                        <a class="main-link"
                                           title="{{ trans('messages.'.$data['listName'].'.metaGlobalTitle', ['siteName' => env('SITE_NAME')]) }}"
                                           href="{{ url('/'.$data['listName']) }}">
                                            <span class="name">{{ trans('messages.randomLinks.'.$data['listName'].'.all') }}</span>
                                            <i class="fa fa-paper-plane"></i>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="footer-links-wrapper">
                <div class="app-row">
                    <div class="links-column app-column large-6 middle-6 small-12">
                        <div class="links-box">
                            <div class="links-box-wrapper">

                                <p class="link-box-header">{{ trans('messages.randomLinks.flights.name') }}</p>

                                @foreach($randomFlights as $flight)
                                    <a class="link-item separated"
                                       title="{{ trans('messages.flights.metaShowTitle', ['from' => $flight['from']['name'], 'to' => $flight['to']['name']])}}"
                                       href="{{ url('/flights/'.$flight['from']['code'].'/'.$flight['to']['code']) }}">
                                        <span>{{  $flight['from']['name'] }}</span>
                                        <i class="fa fa-plane"></i>
                                        <span>{{ $flight['to']['name'] }}</span>
                                    </a>
                                @endforeach

                                @if(url()->current() == url('/flights'))
                                    <div class="link-item separated active">
                                        <span>{{  trans('messages.randomLinks.popularFlights.all') }}</span>
                                        <i class="fa fa-search"></i>
                                    </div>
                                @else
                                    <a class="link-item separated"
                                       title="{{ trans('messages.flights.metaShowTitle') }}"
                                       href="{{ url('/flights') }}">
                                        <span>{{  trans('messages.randomLinks.popularFlights.all') }}</span>
                                        <i class="fa fa-search"></i>
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="links-column app-column large-6 middle-6 small-12">
                        <div class="links-box">
                            <div class="links-box-wrapper">

                                <p class="link-box-header">{{ trans('messages.combinations.title') }}</p>

                                @foreach(trans('messages.combinations.list') as $key => $combination)
                                    @if(url()->current() == url('/'.$combination['link']))
                                        <div class="link-item separated active">
                                            <span>{{  $combination['name'] }}</span>
                                            <i class="{{ $combination['icon'] }}"></i>
                                        </div>
                                    @else
                                        <a class="link-item separated"
                                           title="{{ trans('messages.'.$key.'.metaGlobalTitle', ['siteName' => env('SITE_NAME')]) }}"
                                           href="{{ url('/'.$combination['link']) }}">
                                            <span>{{  $combination['name'] }}</span>
                                            <i class="{{ $combination['icon'] }}"></i>
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-rights">
        <div class="app-fill">
            <div class="footer-rights-wrapper">
                <div class="app-copyright">
                    {{ trans('messages.layouts.copyRight', ['date' => date('Y'),'site' => env('SITE_NAME')]) }}
                </div>
            </div>
        </div>
    </div>
</div>
