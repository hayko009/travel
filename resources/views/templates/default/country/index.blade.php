@extends(config('template').'.layouts.app')

@section('content')
    @if(\Illuminate\Support\Facades\Request::segment(2))
        @include(config('template').'/components/search/index', [
          'title' => trans('messages.countries.pageH1Title', ['page' => $pagination['page'], 'siteName' => env('SITE_NAME')]),
          'paragraph' => trans('messages.countries.pageH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'countries',
                    'url' => 'countries',
                    'props' => []
                ],
                [
                    'name' => 'page',
                    'props' => [
                        'page' => $pagination['page']
                    ]
                ]
            ]
        ])
    @else
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.countries.listH1Title', ['siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.countries.listH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
           'items' => [
               [
                   'name' => 'countries',
                   'props' => []
               ]
           ]
       ])
    @endif

    <div class="content-container">
        <div class="app-fill">
            <h3 class="primary-title">{{ trans('messages.countries.listH3Title') }}</h3>
            <div class="page-content">
                {!! trans('messages.countries.listContent', [
                     "siteName" => env('SITE_NAME'),
                     "siteUrl" => url('/'),
                     "codes" => implode(', ',$countries->pluck('code')->toArray()),
                     "names" => implode(', ',$countries->pluck('name')->toArray()),
                ]) !!}
            </div>
        </div>
    </div>

    <div class="countries-container">
        <div class="list-container countries-list">
            <div class="app-fill">
                @if(\Illuminate\Support\Facades\Request::segment(2))
                    <h4 class="secondary-title">{{ trans("messages.countries.pageH4Title", ["page" => $pagination["page"]]) }}</h4>
                @else
                    <h4 class="secondary-title">{{ trans("messages.countries.listH4Title") }}</h4>
                @endif
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($countries as $country)
                            <div class="country-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.countries.metaShowTitle", ["name" => $country->name]) }}"
                                   href="{{ url('/countries/'.$country->code) }}">
                                    <div class="image-wrapper">
                                        <img alt="{{ trans("messages.countries.metaShowTitle", ["name" => $country->name]) }}"
                                             class="lazy-load"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ url('/images/countries/country.jpg') }}">
                                    </div>
                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.countries.metaShowTitle", ["name" => $country->name]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include (config('template').'/components/pagination/index', [
        'prevPageTitle' => trans("messages.countries.metaPageTitle", ["page" => $pagination["page"] - 1]),
        'currentPageTitle' => trans("messages.countries.pageH1Title", ["page" => $pagination["page"]]),
        'nextPageTitle' => trans("messages.countries.metaPageTitle", ["page" => $pagination["page"] + 1]),
    ])
@endsection
