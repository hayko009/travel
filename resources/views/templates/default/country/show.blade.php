@extends(config('template').'.layouts.app')

@section('content')

    @include(config('template').'/components/search/index', [
        'title' => trans('messages.countries.showH1Title', ['name' => $country->name]),
        'paragraph' => trans('messages.countries.showH2Title', ['name' => $country->name]),
    ])

    @include(config('template').'/components/breadcrumbs/index', [
        'items' => [
            [
                'name' => 'countries',
                'url' => 'countries',
                'props' => []
            ],
            [
                'name' => 'country',
                'props' => [
                    'name' => $country->name
                ]
            ]
        ]
    ])

    @php
        $data = $country->params;

        $airports = [];

        foreach ($country->airports as $airport) {
            $airports[] = "<a title='".trans('messages.airports.metaShowTitle',['name' => $airport->name])."' href='".url('/airports/'.$airport->code)."'>".$airport->name."</a>";
        }

        $cities = [];

        foreach ($country->cities as $city) {
            $cities[] = "<a title='".trans('messages.cities.metaShowTitle',['name' => $city->name])."' href='".url('/cities/'.$city->code)."'>".$city->name."</a>";
        }
    @endphp

    <div class="content-container">
        <div class="app-fill">
            <h3 class="primary-title">{{ trans('messages.countries.showH3Title', ['name' => $country->name]) }}</h3>
            <div class="page-content">
                {!! trans('messages.countries.showContent', [
                     "siteName" => env('SITE_NAME'),
                     "siteUrl" => url('/'),
                     "name" => $country->name,
                     "mapLink" => 'https://www.google.com/maps/embed/v1/place?key='.env('GOOGLE_MAP_KEY').'&q='.str_replace(" ", "+", $country->name).",".str_replace(" ", "+", $country->name."+".$country->name).'&zoom=14&maptype=roadmap',
                     "airportsLinks" => implode(', ', $airports),
                     "airports" => implode(', ', $country->airports->pluck('name')->toArray()),
                     "citiesLinks" => implode(', ', $cities),
                     "cities" => implode(', ', $country->cities->pluck('name')->toArray()),
                     "code" => $country->code,
                     "currency" => $data['currency'],
                ]) !!}
            </div>
        </div>
    </div>

    <div class="countries-container">
        <div class="list-container countries-list">
            <div class="app-fill">
                <h4 class="primary-title">{{ trans('messages.countries.showH4Title') }}</h4>
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($randomCountries as $country)
                            <div class="country-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.countries.metaShowTitle", ["name" => $country->name]) }}"
                                   href="{{ url('/countries/'.$country->code) }}">
                                    <div class="image-wrapper">
                                        <img alt="{{ trans("messages.countries.metaShowTitle", ["name" => $country->name]) }}"
                                             class="lazy-load"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ url('/images/countries/country.jpg') }}">
                                    </div>
                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.countries.metaShowTitle", ["name" => $country->name]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
