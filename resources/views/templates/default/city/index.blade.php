@extends(config('template').'.layouts.app')

@section('content')
    @if(\Illuminate\Support\Facades\Request::segment(2))
        @include(config('template').'/components/search/index', [
          'title' => trans('messages.cities.pageH1Title', ['page' => $pagination['page'], 'siteName' => env('SITE_NAME')]),
          'paragraph' => trans('messages.cities.pageH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'cities',
                    'url' => 'cities',
                    'props' => []
                ],
                [
                    'name' => 'page',
                    'props' => [
                        'page' => $pagination['page']
                    ]
                ]
            ]
        ])
    @else
        @include(config('template').'/components/search/index', [
             'title' => trans('messages.cities.listH1Title', ['siteName' => env('SITE_NAME')]),
             'paragraph' => trans('messages.cities.listH2Title')
        ])

        @include(config('template').'/components/breadcrumbs/index', [
           'items' => [
               [
                   'name' => 'cities',
                   'props' => []
               ]
           ]
       ])
    @endif

    <div class="content-container">
        <div class="app-fill">
            @if(\Illuminate\Support\Facades\Request::segment(2))
                <h3 class="primary-title">{{ trans('messages.cities.pageH3Title') }}</h3>
                <div class="page-content">
                    {!! trans('messages.cities.pageContent', [
                         "siteName" => env('SITE_NAME'),
                         "siteUrl" => url('/'),
                         "codes" => implode(', ', $cities->pluck('code')->toArray()),
                         "names" => implode(', ', $cities->pluck('name')->toArray()),
                    ]) !!}
                </div>
            @else
                <h3 class="primary-title">{{ trans('messages.cities.listH3Title') }}</h3>
                <div class="page-content">
                    {!! trans('messages.cities.listContent', [
                         "siteName" => env('SITE_NAME'),
                         "siteUrl" => url('/'),
                         "codes" => implode(', ', $cities->pluck('code')->toArray()),
                         "names" => implode(', ', $cities->pluck('name')->toArray()),
                    ]) !!}
                </div>
            @endif
        </div>
    </div>

    <div class="cities-container">
        <div class="list-container cities-list">
            <div class="app-fill">
                @if(\Illuminate\Support\Facades\Request::segment(2))
                    <h4 class="secondary-title">{{ trans("messages.cities.pageH4Title", ["page" => $pagination["page"]]) }}</h4>
                @else
                    <h4 class="secondary-title">{{ trans("messages.cities.listH4Title") }}</h4>
                @endif
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($cities as $city)
                            <div class="city-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.cities.metaShowTitle", ["name" => $city->name]) }}"
                                   href="{{ url('/cities/'.$city->code) }}">
                                    <div class="image-wrapper">
                                        <img alt="{{ trans("messages.cities.metaShowTitle", ["name" => $city->name]) }}"
                                             class="lazy-load"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ !empty($city->img)? url('/images/cities/'.$city->img) : url('/images/cities/default.jpg') }}">
                                    </div>
                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.cities.metaShowTitle", ["name" => $city->name]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include (config('template').'/components/pagination/index', [
        'prevPageTitle' => trans("messages.cities.metaPageTitle", ["page" => $pagination["page"] - 1]),
        'currentPageTitle' => trans("messages.cities.pageH1Title", ["page" => $pagination["page"]]),
        'nextPageTitle' => trans("messages.cities.metaPageTitle", ["page" => $pagination["page"] + 1]),
    ])
@endsection
