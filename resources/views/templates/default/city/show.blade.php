@extends(config('template').'.layouts.app')

@section('content')
    @include(config('template').'/components/search/index', [
        'title' => trans('messages.cities.showH1Title', ['name' => $city->name]),
        'paragraph' => trans('messages.cities.showH2Title', ['name' => $city->name]),
        'img' => !empty($city->img) ? url('/images/cities/'.$city->img) : ''
    ])

    @include(config('template').'/components/breadcrumbs/index', [
        'items' => [
            [
                'name' => 'cities',
                'url' => 'cities',
                'props' => []
            ],
            [
                'name' => 'city',
                'props' => [
                    'name' => $city->name
                ]
            ]
        ]
    ])

    @php
        $data = $city->params;

        $airports = [];
        foreach ($city->airports as $airport) {
            $airports[] = "<a title='".trans('messages.airports.metaShowTitle',['name' => $airport->name])."' href='".url('/airports/'.$airport->code)."'>".$airport->name."</a>";
        }
    @endphp

    <div class="content-container">
        <div class="app-fill">
            <h3 class="primary-title">{{ trans('messages.cities.showH3Title', ['name' => $city->name]) }}</h3>
            <div class="page-content">
                {!! trans('messages.cities.showContent', [
                     "siteName" => env('SITE_NAME'),
                     "siteUrl" => url('/'),
                     "name" => $city->name,
                     "mapLink" => 'https://www.google.com/maps/embed/v1/place?key='.env('GOOGLE_MAP_KEY').'&q='.str_replace(" ", "+", $city->name).",".str_replace(" ", "+", $city->country->name."+".$city->name).'&zoom=14&maptype=roadmap',
                     "country" => $city->country->name,
                     "airportsLinks" => implode(', ',$airports),
                     "airports" => implode(', ',$city->airports->pluck('name')->toArray()),
                     "city" => $city->name,
                     "lat" => $data['coordinates']['lat']??'',
                     "lon" => $data['coordinates']['lon']??'',
                     "timeZone" => $data['timeZone'],
                     "code" => $city->code,
                ]) !!}
            </div>
        </div>
    </div>

    <div class="cities-container">
        <div class="list-container cities-list">
            <div class="app-fill">
                <h4 class="secondary-title">{{ trans('messages.cities.showH4Title') }}</h4>
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($randomCities as $city)
                            <div class="city-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.cities.metaShowTitle", ["name" => $city->name]) }}"
                                   href="{{ url('/cities/'.$city->code) }}">
                                    <div class="image-wrapper">
                                        <img alt="{{ trans("messages.cities.metaShowTitle", ["name" => $city->name]) }}"
                                             class="lazy-load"
                                             src="{{ url('/images/loading.gif') }}"
                                             data-lazy="{{ !empty($city->img)? url('/images/cities/'.$city->img) : url('/images/cities/default.jpg') }}">
                                    </div>
                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.cities.metaShowTitle", ["name" => $city->name]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
