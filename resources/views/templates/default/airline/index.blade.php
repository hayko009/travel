@extends(config('template').'.layouts.app')

@section('content')

    @if(\Illuminate\Support\Facades\Request::segment(2))
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.airlines.pageH1Title', ['page' => $pagination['page'], 'siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.airlines.pageH2Title', ['page' => $pagination['page']])
        ])

        @include(config('template').'/components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'airlines',
                    'url' => 'airlines',
                    'props' => []
                ],
                [
                    'name' => 'page',
                    'props' => [
                        'page' => $pagination['page']
                    ]
                ]
            ]
        ])
    @else
        @include(config('template').'/components/search/index', [
            'title' => trans('messages.airlines.listH1Title', ['siteName' => env('SITE_NAME')]),
            'paragraph' => trans('messages.airlines.listH2Title')
        ])

        @include(config('template').'.components/breadcrumbs/index', [
            'items' => [
                [
                    'name' => 'airlines',
                    'props' => []
                ]
            ]
        ])
    @endif

    <div class="content-container">
        <div class="app-fill">
            <h3 class="primary-title">{{ trans('messages.airlines.listH3Title') }}</h3>
            <div class="page-content">
                {!! trans('messages.airlines.listContent', [
                     "siteName" => env('SITE_NAME'),
                     "siteUrl" => url('/'),
                     "codes" => implode(', ', $airlines->pluck('code')->toArray()),
                     "names" => implode(', ', $airlines->pluck('name')->toArray()),
                ]) !!}
            </div>
        </div>
    </div>

    <div class="airlines-container">
        <div class="list-container airlines-list">
            <div class="app-fill">
                @if(\Illuminate\Support\Facades\Request::segment(2))
                    <h4 class="secondary-title">{{ trans("messages.airlines.pageH4Title", ["page" => $pagination["page"]]) }}</h4>
                @else
                    <h4 class="secondary-title">{{ trans("messages.airlines.listH4Title") }}</h4>
                @endif
            </div>

            <div class="list-wrapper">
                <div class="app-fill">
                    <div class="app-row slider-items">
                        @foreach($airlines as $airline)
                            <div class="airline-column app-column large-4 middle-6 small-12">
                                <a class="list-item"
                                   title="{{ trans("messages.airlines.metaShowTitle", ["name" => $airline->name]) }}"
                                   href="{{ url('/airlines/'.$airline->code) }}">
                                    <div class="image-wrapper">
                                        <img alt="{{ trans("messages.airlines.metaShowTitle", ["name" => $airline->name]) }}"
                                             class="lazy-load"
                                             data-lazy="{{ url('/images/airlines/'.$airline->img) }}"
                                             src="{{ url('/images/loading.gif') }}">
                                    </div>
                                    <div class="name-wrapper">
                                        <p class="paragraph">
                                            <strong>{{ trans("messages.airlines.metaShowTitle", ["name" => $airline->name]) }}</strong>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include (config('template').'/components/pagination/index', [
        'prevPageTitle' => trans("messages.airlines.metaPageTitle", ["page" => $pagination["page"] - 1]),
        'currentPageTitle' => trans("messages.airlines.pageH1Title", ["page" => $pagination["page"]]),
        'nextPageTitle' => trans("messages.airlines.metaPageTitle", ["page" => $pagination["page"] + 1]),
    ])
@endsection
