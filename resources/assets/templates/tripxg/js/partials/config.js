window.configs = {
    mobileActivatePoint: 767,
    sliderActivateStatus: false,
    flightsResultsPath: '/flights',
    flightsResultsName: 'flights',

    isMobile: function() {
        return (
            $(window).width() < window.configs.mobileActivatePoint
        )
    },

    sanitizePathsList: function (pathList) {
        let newPathList = [];

        pathList.map((item) => {
            if(item) {
                newPathList.push(item);
            }
        });

        return newPathList;
    }
};
