let locationHashObject = {};

let locationHashParams = window.location.hash.split('&');

locationHashParams.map(function(paramItem) {
    let paramParts = paramItem.split('=');

    if(paramParts[0] && paramParts[1]) {
        locationHashObject[paramParts[0]] = paramParts[1];
    }
});

function locationHashChanged(hash) {
    if(hash.indexOf('marker') <= -1) {
        let hashPaths = configs.sanitizePathsList(hash.split('/'));
        let citiesByHash = hashPaths[2].replace(/\d+/g, ",");
        let citiesList = configs.sanitizePathsList(citiesByHash.split(','));

        let newPath = configs.flightsResultsPath + '/' + citiesList[0] + '/' + citiesList[1];
        if(window.location.pathname !== newPath) {
            window.location.href = window.location.origin + newPath + '/' + hash;
        }
    }
}

if(window.location.pathname === window.configs.flightsResultsPath) {
    if(locationHashObject.origin_iata && locationHashObject.destination_iata) {
        let newLocationUrl = window.location.origin + window.location.pathname + "/";
        window.location.href = newLocationUrl + locationHashObject.origin_iata + "/" + locationHashObject.destination_iata;
    }
}

if(!window.location.hash) {
    if(window.location.pathname) {
        let pathList = window.location.pathname.split('/');

        pathList = configs.sanitizePathsList(pathList);

        if(pathList[0] === configs.flightsResultsName) {
            if(pathList[1] && pathList[2]) {
                let dir_from = pathList[1];
                let dir_to = pathList[2];

                let dateNow = new Date();

                let from_date = dateNow.addDays(1);
                let to_date = dateNow.addDays(7);

                let from_date_list = formatDate(from_date).split('-');
                let to_date_list = formatDate(to_date).split('-');

                let date_from = from_date_list[2] + from_date_list[1];
                let date_to = to_date_list[2] + to_date_list[1];

                let passengers = "1";

                window.location.hash = configs.flightsResultsPath + '/' + dir_from + date_from + dir_to + date_to + passengers;
                window.location.reload();
            }
        }
    }
} else {
    locationHashChanged(window.location.hash);
}

window.addEventListener("message", receiveMessage, false);

function receiveMessage(event) {
    let eventData = JSON.parse(event.data);
    eventData.events.map((eventItem) => {
        switch (eventItem[0]) {
            case 'location_changed':
                locationHashChanged('#' + eventItem[1]);
                break;
        }
    })
}