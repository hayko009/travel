window.$ = window.jQuery = require('jquery');

//node module libs
require('materialize-css');

//require partials
require('./partials/slick-slider');
require('./partials/config');

//require components
require('./componsnets/slider');
require('./componsnets/popular');

//require layouts
require('./layouts/header');

