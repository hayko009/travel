$(document).ready(function () {
    tryToRunSlider();
});

$(window).on('resize', function(){
    tryToRunSlider();
});

function tryToRunSlider() {
    let slickContainer = $('.slider-items');

    if(configs.isMobile()){
        if(!window.configs.sliderActivateStatus) {
            slickContainer.slick({
                dots: false,
                infinite: true,
                lazyLoad: 'ondemand',
                speed: 300,
                slidesToShow: 2,
                slidesToScroll: 2,
                prevArrow: "<button class='slide-button prev-button'><i class='fas fa-angle-left'></i></button>",
                nextArrow: "<button class='slide-button next-button'><i class='fas fa-angle-right'></i></button>",
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
            window.configs.sliderActivateStatus = true;
        }
    } else {
        if(window.configs.sliderActivateStatus){
            slickContainer.slick('unslick');
            window.configs.sliderActivateStatus = false;
        }
    }
}
