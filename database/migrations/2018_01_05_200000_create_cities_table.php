<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id')->unsigned()->notNull();
            $table->integer('country_id')->unsigned()->notNull();
            $table->string('code');
            $table->text('name');
            $table->text('cases');
            $table->integer('ordering')->nullable();
            $table->string('img')->nullable();
            $table->text('params');
            $table->index(['code', 'ordering', 'img']);
            $table->foreign('country_id')
                ->references('id')->on('countries')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('cities');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
