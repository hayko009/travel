<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->increments('id')->unsigned()->notNull();
            $table->integer('from_id')->unsigned()->notNull();
            $table->integer('to_id')->unsigned()->notNull();
            $table->integer('ordering')->nullable();
            $table->index(['ordering']);
            $table->foreign('from_id')
                ->references('id')->on('cities')
                ->onDelete('cascade');
            $table->foreign('to_id')
                ->references('id')->on('cities')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('flights');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
