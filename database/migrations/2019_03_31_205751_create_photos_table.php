<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type')->devsult(0); // city, country ...
            $table->integer('user_id'); // city, country ...
            $table->string('city')->nullable(); // iata dode
            $table->string('country')->nullable(); // iata dode
            $table->string('locale')->nullable();
            $table->string('path')->nullable();
            $table->text('description')->nullable();
            $table->index([
                'type',
                'city',
                'country',
            ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
