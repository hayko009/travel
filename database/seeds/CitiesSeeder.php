<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        DB::table('cities')->truncate();
        $languages = explode(',', env('LANGUAGES'));
        $citiesData = [];
        $citiesLanguages = [];

        foreach ($languages as $lang) {
            $citiesLanguages[$lang] = json_decode(file_get_contents(env('TRAVELPAYOUTS_URL').$lang.'/cities.json'),
                true);
        }

        foreach ($citiesLanguages as $lang => $cities) {
            foreach ($cities as $city) {
                if (!empty($city['code']) && (!empty($city['name']) || !empty($city['name_translations']['en']))) {
                    $citiesData[$city['code']]['code'] = $city['code'];
                    $citiesData[$city['code']]['countryCode'] = $city['country_code'];
                    $citiesData[$city['code']]['params'] = [
                        'coordinates' => $city['coordinates'],
                        'timeZone'    => $city['time_zone']
                    ];
                    $citiesData[$city['code']]['name'][$lang] = !empty($city['name']) ? $city['name'] : $city['name_translations']['en'];
                    if (!empty($city['cases'])
                        && !empty($city['cases']['vi'])
                        && !empty($city['cases']['tv'])
                        && !empty($city['cases']['ro'])
                        && !empty($city['cases']['pr'])
                        && !empty($city['cases']['da'])) {
                        $citiesData[$city['code']]['cases'][$lang] = $city['cases'];
                    } else {
                        $citiesData[$city['code']]['cases'][$lang] = null;
                    }
                }
            }
        }

        $bigData = [];
        $cities = array_values($citiesData);
        $this->command->getOutput()->progressStart(count($cities));
        foreach ($cities as $key => $city) {
            if ($country = Country::where('code', $city['countryCode'])->first()) {
                $bigData[] = [
                    'country_id' => $country->id,
                    'code'       => $city['code'],
                    'name'       => json_encode($city['name']),
                    'cases'      => json_encode($city['cases']),
                    'params'     => json_encode($city['params']),
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ];
            }

            if (count($bigData) == 2000 || $key == count($cities) - 1) {
                DB::table('cities')->insert($bigData);
                $bigData = [];
            }

            $this->command->getOutput()->progressAdvance();
        }
        $this->command->getOutput()->progressFinish();
    }

}
