<?php

use Illuminate\Database\Seeder;
use App\Models\City;
use App\Models\Flight;

class FlightsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        DB::table('flights')->truncate();
        $cities = City::where('exists_cases', 1)->whereNotNull('img')->get();
        $bigData = [];
        $this->command->getOutput()->progressStart(count($cities));
        foreach ($cities as $key1 => $city1) {
            foreach ($cities as $key2 => $city2) {
                if ($city1->id != $city2->id) {
                    $bigData[] = [
                        'from_id'    => $city1->id,
                        'to_id'      => $city2->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                    ];
                }

                if (count($bigData) == 10000 || ($key2 == count($cities) - 1 && $key1 == count($cities) - 1)) {
                    Flight::insert($bigData);
                    $bigData = [];
                }
            }
            $this->command->getOutput()->progressAdvance();
        }
        $this->command->getOutput()->progressFinish();
    }
}
