<?php

use Illuminate\Database\Seeder;

class AirlinesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        DB::table('airlines')->truncate();

        $languages = explode(',', env('LANGUAGES'));
        $airlinesData = [];
        $airlinesLanguages = [];

        foreach ($languages as $lang) {
            $airlinesLanguages[$lang] = json_decode(file_get_contents(env('TRAVELPAYOUTS_URL').$lang.'/airlines.json'),
                true);
        }

        foreach ($airlinesLanguages as $lang => $airlines) {
            foreach ($airlines as $airline) {
                if (!empty($airline['code']) && (!empty($airline['name']) || !empty($airline['name_translations']['en']))) {
                    $airlinesData[$airline['code']]['code'] = $airline['code'];
                    $airlinesData[$airline['code']]['name'][$lang] = !empty($airline['name']) ? $airline['name'] : $airline['name_translations']['en'];

                }
            }
        }

        $bigData = [];
        $airlines = array_values($airlinesData);

        $this->command->getOutput()->progressStart(count($airlines));
        foreach ($airlines as $key => $airline) {

            $bigData[] = [
                'code'       => $airline['code'],
                'name'       => json_encode($airline['name']),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ];

            if (count($bigData) == 100 || $key == count($airlines) - 1) {
                DB::table('airlines')->insert($bigData);
                $bigData = [];
            }

            $this->command->getOutput()->progressAdvance();
        }
        $this->command->getOutput()->progressFinish();

    }
}
