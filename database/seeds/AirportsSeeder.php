<?php

use Illuminate\Database\Seeder;
use App\Models\City;

class AirportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        DB::table('airports')->truncate();

        $languages = explode(',', env('LANGUAGES'));
        $airportsData = [];
        $airportsLanguages = [];

        foreach ($languages as $lang) {
            $airportsLanguages[$lang] = json_decode(file_get_contents(env('TRAVELPAYOUTS_URL').$lang.'/airports.json'),
                true);
        }

        foreach ($airportsLanguages as $lang => $airports) {
            foreach ($airports as $airport) {
                if (!empty($airport['code']) && (!empty($airport['name']) || !empty($airport['name_translations']['en']))) {
                    $airportsData[$airport['code']]['code'] = $airport['code'];
                    $airportsData[$airport['code']]['name'][$lang] = !empty($airport['name']) ? $airport['name'] : $airport['name_translations']['en'];
                    $airportsData[$airport['code']]['countryCode'] = $airport['country_code'];
                    $airportsData[$airport['code']]['cityCode'] = $airport['city_code'];
                    $airportsData[$airport['code']]['params'] = [
                        'coordinates' => $airport['coordinates'],
                        'timeZone'    => $airport['timeZone'],
                        'flightable'  => $airport['flightable']
                    ];
                }
            }
        }

        $bigData = [];
        $airports = array_values($airportsData);
        $this->command->getOutput()->progressStart(count($airports));
        foreach ($airports as $key => $airport) {
            if ($city = City::where('code', $airport['cityCode'])->first()) {
                $bigData[] = [
                    'city_id'    => $city->id,
                    'country_id' => $city->country->id,
                    'code'       => $airport['code'],
                    'name'       => json_encode($airport['name']),
                    'params'     => json_encode($airport['params']),
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ];
            }

            if (count($bigData) == 2000 || $key == count($airports) - 1) {
                DB::table('airports')->insert($bigData);
                $bigData = [];
            }

            $this->command->getOutput()->progressAdvance();
        }
        $this->command->getOutput()->progressFinish();
    }
}
