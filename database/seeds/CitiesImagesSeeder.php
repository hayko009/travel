<?php

use Illuminate\Database\Seeder;

use App\Models\City;

class CitiesImagesSeeder extends Seeder
{
    /**
     * Download image url
     * @var string
     */
    protected $downloadImageUrl = 'http://photo.hotellook.com/static/cities/1200x900/';

    /**
     * Image path save
     * @var string
     */
    protected $imageSavePath = 'public/images/cities/';
    protected $originalImageSavePath = 'public/images/cities-original-images/';

    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        $files = glob($this->imageSavePath . '*.jpg'); //get all file names
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file); //delete file
        }

        // Download default image and get ToDO
        //$defaultImage = $this->downloadDefaultImage();

        // Get all cities
        $cities = City::with('country')->get();

        copy($this->originalImageSavePath . 'default.jpg', $this->imageSavePath . "default.jpg");

        $this->command->getOutput()->progressStart(count($cities));
        foreach ($cities as $key => $city) {

            $fileName = str_slug($city->name) . '-' . $city->code . '-' . time() . '-' . str_slug($city->country->name) . '-' . $city->country->code . '.jpg';

            // Build image path
            $imageSavePath = $this->imageSavePath . $fileName;

            if (file_exists($this->originalImageSavePath . $city->code . '.jpg')) {
                copy($this->originalImageSavePath . $city->code . '.jpg', $imageSavePath);
                $city->img = $fileName;
                $city->save();
            } else {

                // Get image data Todo
                //$image = $this->getImageData($this->downloadImageUrl . $city->code . ".auto");

                //if ($image != $defaultImage) {
                //    file_put_contents($imageSavePath, $image);
                //    $city->img = $fileName;
                //    $city->save();
                //}
            }
            $this->command->getOutput()->progressAdvance();
        }
        $this->command->getOutput()->progressFinish();
    }

    /**
     * Download Default image
     * @return bool|mixed
     */
    protected function downloadDefaultImage()
    {
        // Get image image from url
        $defaultImage = $this->getImageData($this->downloadImageUrl . 'default.auto');

        // Check exists image
        if (!$defaultImage) {
            $this->command->line('<fg=red>Default Image Not found!</>');
            exit;
        }

        // Save image
        file_put_contents($this->imageSavePath . 'default.jpg', $defaultImage);

        return $defaultImage;
    }

    /**
     * Get Image Data
     * @param $url
     * @return bool|mixed
     */
    protected function getImageData($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        if (!curl_errno($ch) && $code == 200) {
            curl_close($ch);
            return $body;
        } else {
            curl_close($ch);
            return false;
        }
    }
}
