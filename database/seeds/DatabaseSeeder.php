<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(CountriesSeeder::class);
        $this->call(AirlinesSeeder::class);
        $this->call(AirlinesImagesSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(AirportsSeeder::class);
        $this->call(CitiesImagesSeeder::class);
        //$this->call(FlightsSeeder::class);
        $this->call(SiteSettingsSeeder::class);
        $this->call(VideosSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
