<?php

use Illuminate\Database\Seeder;
use App\Models\Airline;

class AirlinesImagesSeeder extends Seeder
{
    /**
     * Download image url
     * @var string
     */
    protected $downloadImageUrl = 'https://daisycon.io/images/airline/?width=200&height=200&color=fff&iata=';

    /**
     * Image path save
     * @var string
     */
    protected $originalImageSavePath = 'public/images/airlines-original-images/';
    protected $imageSavePath = 'public/images/airlines/';

    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {

        // Download default image and get ToDO
        //$defaultImage = $this->downloadDefaultImage();

        // Get all cities
        $airlines = Airline::get();

        $files = glob($this->imageSavePath . '*.jpg'); //get all file names
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file); //delete file
        }

        copy($this->originalImageSavePath . 'default.jpg', $this->imageSavePath . "default.jpg");

        $this->command->getOutput()->progressStart(count($airlines));
        foreach ($airlines as $key => $airline) {

            $fileName = str_slug($airline->name) . '-' . time() . '-' . $airline->code . '.jpg';

            // Build image path
            $imageSavePath = $this->imageSavePath . $fileName;

            if (file_exists($this->originalImageSavePath . $airline->code . '.jpg')) {
                copy($this->originalImageSavePath . $airline->code . '.jpg', $imageSavePath);
            } else {
                $fileName = 'default.jpg';
            }
            //$originalImageSavePath = $this->originalImageSavePath . $airline->code . '.jpg';

            // Get image data
//            $image = $this->getImageData($this->downloadImageUrl . $airline->code);
//
//            if ($image != $defaultImage) {
//                file_put_contents($imageSavePath, $image);
//                file_put_contents($originalImageSavePath, $image);
//            } else {
//                $fileName = "default.jpg";
//                file_put_contents($this->imageSavePath . 'default.jpg', $defaultImage);
//            }
//
            $airline->img = $fileName;
            $airline->save();

            $this->command->getOutput()->progressAdvance();
        }
        $this->command->getOutput()->progressFinish();
    }

    /**
     * Download Default image
     * @return bool|mixed
     */
    protected function downloadDefaultImage()
    {
        // Get image image from url
        $defaultImage = $this->getImageData($this->downloadImageUrl . 'default');

        // Check exists image
        if (!$defaultImage) {
            $this->command->line('<fg=red>Default Image Not found!</>');
            exit;
        }

        // Save image
        file_put_contents($this->originalImageSavePath . 'default.jpg', $defaultImage);

        return $defaultImage;
    }

    /**
     * Get Image Data
     * @param $url
     * @return bool|mixed
     */
    protected function getImageData($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        if (!curl_errno($ch) && $code == 200) {
            curl_close($ch);
            return $body;
        } else {
            curl_close($ch);
            return false;
        }
    }
}
