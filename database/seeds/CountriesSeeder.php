<?php

use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        DB::table('countries')->truncate();

        $languages = explode(',',env('LANGUAGES'));
        $countriesData = [];
        $countriesLanguages = [];

        foreach ($languages as $lang) {
            $countriesLanguages[$lang] = json_decode(file_get_contents(env('TRAVELPAYOUTS_URL').$lang.'/countries.json'), true);
        }

        foreach ($countriesLanguages as $lang => $countries) {
            foreach ($countries as $country) {
                if (!empty($country['code']) && !empty($country['name'])) {
                    $countriesData[$country['code']]['code'] = $country['code'];
                    $countriesData[$country['code']]['params'] = [
                        'currency' => $country['currency']
                    ];
                    $countriesData[$country['code']]['name'][$lang] = $country['name'];
                    if (!empty($country['cases'])
                        && !empty($country['cases']['vi'])
                        && !empty($country['cases']['tv'])
                        && !empty($country['cases']['ro'])
                        && !empty($country['cases']['pr'])
                        && !empty($country['cases']['da'])) {
                        $countriesData[$country['code']]['cases'][$lang] = $country['cases'];
                    } else {
                        $countriesData[$country['code']]['cases'][$lang] = null;
                    }
                }
            }
        }

        $bigData = [];
        $countries = array_values($countriesData);
        $this->command->getOutput()->progressStart(count($countries));
        foreach ($countries as $key => $country) {
            $bigData[] = [
                'code'         => $country['code'],
                'name'         => json_encode($country['name']),
                'params'       => json_encode($country['params']),
                'cases'        => json_encode($country['cases']),
                'created_at'   => date("Y-m-d H:i:s"),
                'updated_at'   => date("Y-m-d H:i:s"),
            ];

            if (count($bigData) == 100 || $key == count($countries) - 1) {
                DB::table('countries')->insert($bigData);
                $bigData = [];
            }

            $this->command->getOutput()->progressAdvance();
        }
        $this->command->getOutput()->progressFinish();
    }
}
