<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiteSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('site_settings')->truncate();
        DB::table('site_settings')->insert([
            [
                'key' => 'airlinesCount',
                'value' => DB::table('airlines')->count(),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'key' => 'airportsCount',
                'value' => DB::table('airports')->count(),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'key' => 'countriesCount',
                'value' => DB::table('countries')->count(),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'key' => 'citiesCount',
                'value' => DB::table('cities')->count(),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'key' => 'fullCitiesCount',
                'value' => \App\Models\City::whereNotNull('img')->count(),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'key' => 'flightsCount',
                'value' => DB::table('flights')->count(),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        ]);

    }
}
